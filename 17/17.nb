(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     10142,        290]
NotebookOptionsPosition[      9024,        259]
NotebookOutlinePosition[      9593,        280]
CellTagsIndexPosition[      9550,        277]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 17, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, {3.72478326559545*^9, 
   3.724783265842194*^9}, {3.7247833377094812`*^9, 
   3.724783338764159*^9}},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:041f\:043e\:043a\:0430\:0437\:0430\:0442\:044c \:0434\:0432\:0443\:043c\
\:044f \:0441\:043f\:043e\:0441\:043e\:0431\:0430\:043c\:0438, \:0447\:0442\
\:043e \:043e\:043f\:0435\:0440\:0430\:0442\:043e\:0440 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"\[CapitalPhi]", ":", " ", 
     RowBox[{"C", "[", 
      RowBox[{"a", ";", "b"}], "]"}]}], "\[Rule]", 
    RowBox[{"C", "[", 
     RowBox[{"a", ";", "b"}], "]"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "428b5b84-576e-4f2c-bde5-64cf47cf1aa0"],
 " \:044f\:0432\:043b\:044f\:0435\:0442\:0441\:044f \:0441\:0436\:0438\:043c\
\:0430\:044e\:0449\:0438\:043c:\nA. \:043f\:043e \:043e\:043f\:0440\:0435\
\:0434\:0435\:043b\:0435\:043d\:0438\:044e \:0441\:0436\:0438\:043c\:0430\
\:044e\:0449\:0435\:0433\:043e \:043e\:043f\:0435\:0440\:0430\:0442\:043e\
\:0440\:0430\nB. \:043f\:043e \
\:0434\:043e\:0441\:0442\:0430\:0442\:043e\:0447\:043d\:043e\:043c\:0443 \
\:043f\:0440\:0438\:0437\:043d\:0430\:043a\:0443 \:0441\:0436\:0438\:043c\
\:0430\:044e\:0449\:0435\:0433\:043e \:043e\:043f\:0435\:0440\:0430\:0442\
\:043e\:0440\:0430"
}], "Text",
 CellChangeTimes->{{3.724787353037389*^9, 
  3.724787404113205*^9}},ExpressionUUID->"48646fe3-5a2a-4d96-915a-\
173150a321da"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"\[CapitalPhi]", "[", "x", "]"}], "=", 
    RowBox[{
     FractionBox[
      RowBox[{"cos", "(", 
       RowBox[{"x", "(", "t", ")"}], ")"}], 
      RowBox[{"t", "+", "2"}]], "+", 
     RowBox[{"sin", "(", "t", ")"}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "a75fc374-01ae-4660-8422-d20bdface25f"],
 ", ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"[", 
     RowBox[{"a", ";", "b"}], "]"}], "=", 
    RowBox[{"[", 
     RowBox[{"0", ";", 
      RowBox[{"3", "\[Pi]"}]}], "]"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "97c5c35c-86ac-45bc-9cb1-3b77e81a4114"]
}], "Text",
 InitializationCell->True,
 CellChangeTimes->{{3.7247874187405357`*^9, 3.724787463252181*^9}, {
  3.7247909294621058`*^9, 
  3.724790930127253*^9}},ExpressionUUID->"f24ff827-1b7c-4cdb-9f08-\
22111bde1801"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435 A", "Section",
 CellChangeTimes->{{3.716646160199793*^9, 3.716646165043662*^9}, {
  3.716734600686346*^9, 3.716734608587257*^9}, {3.724778522397407*^9, 
  3.724778523736287*^9}, {3.724787510464349*^9, 
  3.7247875147465143`*^9}},ExpressionUUID->"8c178dec-d468-439d-aef4-\
0f7d38b23ab3"],

Cell[TextData[Cell[BoxData[{
 FormBox[
  RowBox[{
   RowBox[{
    SubscriptBox["\[Rho]", 
     RowBox[{"C", "[", 
      RowBox[{
       RowBox[{"-", "1"}], ";", "1"}], "]"}]], "(", 
    RowBox[{
     RowBox[{"\[CapitalPhi]", "[", "x", "]"}], ",", 
     RowBox[{"\[CapitalPhi]", "[", "y", "]"}]}], ")"}], " ", "=", " ", 
   RowBox[{
    RowBox[{
     SubscriptBox["max", 
      RowBox[{"t", "\[Element]", 
       RowBox[{"[", 
        RowBox[{"\[Pi]", ";", 
         RowBox[{"3", "\[Pi]"}]}], "]"}]}]], "|", 
     RowBox[{
      RowBox[{"\[CapitalPhi]", "[", "x", "]"}], "-", 
      RowBox[{"\[CapitalPhi]", "[", "y", "]"}]}], "|"}], "=", 
    "\[IndentingNewLine]", "          ", 
    RowBox[{"=", 
     RowBox[{
      RowBox[{
       SubscriptBox["max", 
        RowBox[{"t", "\[Element]", 
         RowBox[{"[", 
          RowBox[{"0", ";", 
           RowBox[{"3", "\[Pi]"}]}], "]"}]}]], "|", 
       RowBox[{
        FractionBox[
         RowBox[{"cos", "(", 
          RowBox[{"x", "(", "t", ")"}], ")"}], 
         RowBox[{"t", "+", "2"}]], "-", 
        FractionBox[
         RowBox[{"cos", "(", 
          RowBox[{"y", "(", "t", ")"}], ")"}], 
         RowBox[{"t", "+", "2"}]]}], "|"}], "=", "\[IndentingNewLine]", 
      RowBox[{"=", 
       RowBox[{
        SubscriptBox["max", 
         RowBox[{"t", "\[Element]", 
          RowBox[{"[", 
           RowBox[{"0", ";", 
            RowBox[{"3", "\[Pi]"}]}], "]"}]}]], 
        FractionBox[
         RowBox[{"|", 
          RowBox[{
           RowBox[{"cos", "(", 
            RowBox[{"x", "(", "t", ")"}], ")"}], "-", 
           RowBox[{"cos", "(", 
            RowBox[{"y", "(", "t", ")"}], ")"}]}], "|"}], 
         RowBox[{"t", "+", "2"}]]}]}]}]}]}]}], 
  TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"                     ", 
   RowBox[{"=", 
    RowBox[{
     SubscriptBox["max", 
      RowBox[{"t", "\[Element]", 
       RowBox[{"[", 
        RowBox[{"0", ";", 
         RowBox[{"3", "\[Pi]"}]}], "]"}]}]], "2", 
     FractionBox[
      RowBox[{"|", 
       RowBox[{"sin", 
        FractionBox[
         RowBox[{
          RowBox[{"x", "(", "t", ")"}], "+", 
          RowBox[{"y", 
           RowBox[{"(", "t", ")"}]}]}], "2"], "sin", 
        FractionBox[
         RowBox[{
          RowBox[{"x", "(", "t", ")"}], "-", 
          RowBox[{"y", "(", "t", ")"}]}], "2"]}], "|"}], 
      RowBox[{"t", "+", "2"}]]}]}]}], TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"                    ", 
   RowBox[{"\[LessEqual]", 
    RowBox[{"2", 
     SubscriptBox["max", 
      RowBox[{"t", "\[Element]", 
       RowBox[{"[", 
        RowBox[{"0", ";", 
         RowBox[{"3", "\[Pi]"}]}], "]"}]}]], 
     FractionBox[
      RowBox[{"|", 
       RowBox[{"sin", 
        FractionBox[
         RowBox[{
          RowBox[{"x", "(", "t", ")"}], "+", 
          RowBox[{"y", 
           RowBox[{"(", "t", ")"}]}]}], "2"]}], "|"}], 
      RowBox[{"t", "+", "2"}]], 
     SubscriptBox["max", 
      RowBox[{"t", "\[Element]", 
       RowBox[{"[", 
        RowBox[{"\[Pi]", ";", 
         RowBox[{"3", "\[Pi]"}]}], "]"}]}]], 
     FractionBox[
      RowBox[{"|", 
       RowBox[{"sin", 
        FractionBox[
         RowBox[{
          RowBox[{"x", "(", "t", ")"}], "-", 
          RowBox[{"y", "(", "t", ")"}]}], "2"]}], "|"}], 
      RowBox[{"t", "+", "2"}]]}]}]}], TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"                    ", 
   RowBox[{"\[LessEqual]", 
    RowBox[{"2", " ", 
     FractionBox["1", "2"], " ", 
     SubscriptBox["max", 
      RowBox[{"t", "\[Element]", 
       RowBox[{"[", 
        RowBox[{"\[Pi]", ";", 
         RowBox[{"3", "\[Pi]"}]}], "]"}]}]], 
     FractionBox[
      RowBox[{"|", 
       RowBox[{"sin", 
        FractionBox[
         RowBox[{
          RowBox[{"x", "(", "t", ")"}], "-", 
          RowBox[{"y", "(", "t", ")"}]}], "2"]}], "|"}], 
      RowBox[{"t", "+", "2"}]]}]}]}], TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"\t       ", 
   RowBox[{"\[LessEqual]", " "}]}], TraditionalForm]}],
 InitializationCell->True,
 FormatType->
  "TraditionalForm",ExpressionUUID->"f62b0bfd-dc90-4663-b205-5a5bec28dae7"]], \
"Text",
 CellChangeTimes->{{3.724787682744055*^9, 3.7247878257946033`*^9}, {
  3.724787891367309*^9, 3.724788084988406*^9}, {3.724788120891199*^9, 
  3.7247881405149403`*^9}, {3.724788261952421*^9, 3.724788265984128*^9}, {
  3.724788297584599*^9, 3.724788324069955*^9}, {3.7247883551740913`*^9, 
  3.7247883667996807`*^9}, {3.724790393269845*^9, 3.724790498126216*^9}, {
  3.7247905512408648`*^9, 3.7247905543373413`*^9}, {3.724790644996809*^9, 
  3.7247906545888977`*^9}, {3.724790741702162*^9, 3.7247907839261293`*^9}, {
  3.724790835373892*^9, 3.724790843832707*^9}, {3.724790934843108*^9, 
  3.724790985205961*^9}, {3.724791032275939*^9, 3.724791105518674*^9}, {
  3.724791149091511*^9, 
  3.724791155545944*^9}},ExpressionUUID->"1268239d-4ea5-4931-8e37-\
b71468ac38b9"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{958, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/print.pdf"},
Magnification:>1.25 Inherited,
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 687, 11, 118, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1292, 37, 187, 3, 84, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[1482, 42, 1273, 26, 102, "Text",ExpressionUUID->"48646fe3-5a2a-4d96-915a-173150a321da"],
Cell[2758, 70, 915, 29, 50, "Text",ExpressionUUID->"f24ff827-1b7c-4cdb-9f08-22111bde1801",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[3710, 104, 335, 5, 84, "Section",ExpressionUUID->"8c178dec-d468-439d-aef4-0f7d38b23ab3"],
Cell[4048, 111, 4948, 144, 308, "Text",ExpressionUUID->"1268239d-4ea5-4931-8e37-b71468ac38b9"]
}, Open  ]]
}, Open  ]]
}
]
*)

