(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     23538,        622]
NotebookOptionsPosition[     21221,        574]
NotebookOutlinePosition[     21788,        595]
CellTagsIndexPosition[     21745,        592]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 41, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, 3.717786296159808*^9, {
   3.71786504637932*^9, 3.7178650467360163`*^9}, {3.717919799320438*^9, 
   3.717919799615849*^9}, {3.71792363608844*^9, 3.717923636650264*^9}, {
   3.718379681596424*^9, 3.718379682353087*^9}, {3.718381431981628*^9, 
   3.718381432046639*^9}, 3.718383638306239*^9, {3.7184457459501038`*^9, 
   3.718445746210128*^9}, {3.718517124880769*^9, 3.718517125403768*^9}, {
   3.718789938653181*^9, 3.7187899401331377`*^9}, {3.718869606189335*^9, 
   3.718869607546228*^9}, {3.718887719478931*^9, 3.7188877211009283`*^9}, {
   3.718897489962224*^9, 3.718897490209896*^9}, {3.719128396741603*^9, 
   3.719128397009714*^9}, {3.7195636414291677`*^9, 
   3.719563641895688*^9}},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:041f\:0440\:043e\:0432\:0435\:0440\:0438\:0442\:044c, \:044f\:0432\:043b\
\:044f\:0435\:0442\:0441\:044f \:043b\:0438 \:043e\:043f\:0435\:0440\:0430\
\:0442\:043e\:0440 ",
 Cell[BoxData[
  FormBox["A", TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "57fc90dc-78b4-4e6d-929a-b6637bc4847d"],
 " \:043b\:0438\:043d\:0435\:0439\:043d\:044b\:043c. \:0414\:043b\:044f \
\:0434\:0430\:043d\:043d\:043e\:0439 \:0444\:0443\:043d\:043a\:0446\:0438\
\:0438 ",
 Cell[BoxData[
  FormBox["x", TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "3397d146-559f-4ea1-bc36-6349fb05ed68"],
 " \:043d\:0430\:0439\:0442\:0438 \:0437\:043d\:0430\:0447\:0435\:043d\:0438\
\:0435 A[x]."
}], "Text",
 CellChangeTimes->{{3.719563648890119*^9, 
  3.719563682634356*^9}},ExpressionUUID->"16862608-bc0d-41bc-abc3-\
86ba35c82cd4"],

Cell[TextData[{
 "a) A: C[-1;1]\[Rule]C[-1;1], ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"A", "(", "x", ")"}], "=", 
    RowBox[{
     SubsuperscriptBox["\[Integral]", 
      RowBox[{"-", "1"}], "t"], 
     RowBox[{
      SuperscriptBox["e", 
       RowBox[{"x", "(", "s", ")"}]], 
      RowBox[{"\[DifferentialD]", "s"}]}]}]}], TraditionalForm]],
  ExpressionUUID->"fc7c5b21-9e91-4b4e-8af2-7a8cc2e21281"],
 ", ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"x", "(", "t", ")"}], "=", 
    RowBox[{
     RowBox[{"3", "t"}], "+", "5"}]}], TraditionalForm]],ExpressionUUID->
  "2e04218f-7222-44ff-8670-597ef9018caa"],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"b", ")"}], 
      RowBox[{"A", ":", 
       RowBox[{
        SuperscriptBox["C", "2"], "[", 
        RowBox[{"0", ";", "1"}], "]"}]}]}], "\[Rule]", 
     RowBox[{"C", "[", 
      RowBox[{"0", ";", "1"}], "]"}]}], ",", " ", 
    RowBox[{
     RowBox[{"A", "[", "x", "]"}], "=", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["t", "2"], "-", "1"}], ")"}], 
       RowBox[{"x", "''"}], 
       RowBox[{"(", "t", ")"}]}], "+", 
      RowBox[{"2", 
       RowBox[{"tx", "'"}], 
       RowBox[{"(", "t", ")"}]}]}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "2ce2f534-10db-4f82-ba8f-3f9165f36b9d"],
 ", ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"x", "(", "t", ")"}], "=", 
    RowBox[{
     SuperscriptBox["t", "3"], "-", 
     RowBox[{"3", 
      SuperscriptBox["t", "2"]}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "4abd6011-6b42-4971-8f8f-f9e32825ec6a"]
}], "Text",
 CellChangeTimes->{{3.719563733332034*^9, 3.719563855393876*^9}, {
  3.719564222991363*^9, 3.7195642229916*^9}, {3.7207806594189672`*^9, 
  3.72078066878086*^9}, {3.720780742984639*^9, 3.720780797376684*^9}, {
  3.720781539040345*^9, 
  3.720781541298747*^9}},ExpressionUUID->"0b17c59d-7944-4125-b3fa-\
b837a331a638"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435 \:043f\:0443\:043d\:043a\
\:0442\:0430 a", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 3.716642603163493*^9}, {
  3.719563702090982*^9, 3.719563703767712*^9}, {3.719564239235032*^9, 
  3.719564241607011*^9}},ExpressionUUID->"3b32b734-bf0a-4546-be02-\
6bb86c53665d"],

Cell[TextData[{
 "\:041f\:0440\:043e\:0432\:0435\:0440\:0438\:043c \:0432\:044b\:043f\:043e\
\:043b\:043d\:0435\:043d\:0438\:0435 \:0443\:0442\:0432\:0435\:0440\:0436\
\:0434\:0435\:043d\:0438\:044f ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"A", "[", 
     RowBox[{
      SubscriptBox["x", "1"], "+", 
      SubscriptBox["x", "2"]}], "]"}], "=", 
    RowBox[{
     RowBox[{"A", "[", 
      SubscriptBox["x", "1"], "]"}], "+", 
     RowBox[{"A", "[", 
      SubscriptBox["x", "2"], "]"}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "2685c793-82ef-493e-9b60-3270059e6085"],
 ":"
}], "Text",
 CellChangeTimes->{{3.719564369832242*^9, 3.719564384408202*^9}, {
  3.7195645888246603`*^9, 3.719564593241581*^9}, {3.7207798970839977`*^9, 
  3.720779991302093*^9}},ExpressionUUID->"ac9b598f-a538-4b43-9070-\
7bc651f96fe1"],

Cell[TextData[Cell[BoxData[{
 FormBox[
  RowBox[{
   RowBox[{"A", "[", 
    RowBox[{
     SubscriptBox["x", "1"], "+", 
     SubscriptBox["x", "2"]}], "]"}], "=", 
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "1"}], "t"], 
    RowBox[{
     SuperscriptBox["e", 
      RowBox[{
       RowBox[{
        SubscriptBox["x", "1"], "(", "s", ")"}], "+", 
       RowBox[{
        SubscriptBox["x", "2"], "(", "s", ")"}]}]], 
     RowBox[{"\[DifferentialD]", "s"}]}]}]}], 
  TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"\[NotEqual]", 
   RowBox[{
    RowBox[{
     SubsuperscriptBox["\[Integral]", 
      RowBox[{"-", "1"}], "t"], 
     RowBox[{
      SuperscriptBox["e", 
       RowBox[{
        SubscriptBox["x", "1"], "(", "s", ")"}]], 
      RowBox[{"\[DifferentialD]", "s"}]}]}], "+", 
    RowBox[{
     SubsuperscriptBox["\[Integral]", 
      RowBox[{"-", "1"}], "t"], 
     RowBox[{
      SuperscriptBox["e", 
       RowBox[{
        SubscriptBox["x", "2"], "(", "s", ")"}]], 
      RowBox[{"\[DifferentialD]", "s"}]}]}]}]}], TraditionalForm]}],
 FormatType->
  "TraditionalForm",ExpressionUUID->"6624cdd8-cf3b-4b0a-b909-5ca671cdfb8e"]], \
"Text",
 CellChangeTimes->{{3.720779992861618*^9, 3.720780017854251*^9}, {
  3.7207800522710857`*^9, 3.720780128583242*^9}, {3.720780168024652*^9, 
  3.7207801864308567`*^9}, {3.72078103722014*^9, 
  3.720781037220315*^9}},ExpressionUUID->"e62c69e3-ee42-47f8-bfd9-\
282136c1ba74"],

Cell["\<\
\:0412 \:043e\:0431\:0449\:0435\:043c \:0441\:043b\:0443\:0447\:0430\:0435 \
\:043e\:0442\:043d\:043e\:0448\:0435\:043d\:0438\:0435 \:043d\:0435 \:0432\
\:044b\:043f\:043e\:043b\:043d\:044f\:0435\:0442\:0441\:044f (\:043d\:0435\
\:0432\:043e\:0437\:043c\:043e\:0436\:043d\:043e \:043f\:0440\:0435\:043e\
\:0431\:0440\:0430\:0437\:043e\:0432\:0430\:0442\:044c \:0438\:043d\:0442\
\:0435\:0433\:0440\:0430\:043b \:044d\:043a\:0441\:043f\:043e\:043d\:0435\
\:043d\:0442\:044b, \:0432\:043e\:0437\:0432\:0435\:0434\:0435\:043d\:043d\
\:043e\:0439 \:0432 \:0441\:0442\:0435\:043f\:0435\:043d\:044c, \:0437\:0430\
\:0434\:0430\:043d\:043d\:0443\:044e \:0441\:0443\:043c\:043c\:043e\:0439 \
\:043d\:0435\:0438\:0437\:0432\:0435\:0441\:0442\:043d\:044b\:0445 \:0444\
\:0443\:043d\:043a\:0446\:0438\:0439 \:0432 \:0441\:0443\:043c\:043c\:0443 \
\:0438\:043d\:0442\:0435\:0433\:0440\:0430\:043b\:043e\:0432).\
\>", "Text",
 CellChangeTimes->{{3.720780244476317*^9, 
  3.7207803419813747`*^9}},ExpressionUUID->"fa59c24b-16d1-4112-a7ba-\
0b4dc348677f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Print", "[", 
  RowBox[{"\"\<\!\(\*Cell[TextData[{
\"A[x(t)]=\",
Cell[BoxData[FormBox[RowBox[{
SubsuperscriptBox[\"\[Integral]\", RowBox[{\"-\", \"1\"}], \"t\"], RowBox[{
SuperscriptBox[\"e\", RowBox[{RowBox[{\"3\", \"s\"}], \"+\", \"5\"}]], \
RowBox[{\"\[DifferentialD]\", \"s\"}]}]}], TraditionalForm]],ExpressionUUID->\
\"97e5a5c8-7d8d-499f-ac79-b833f0756ab9\"],
\" = \"
}],ExpressionUUID->\"fe4c0442-9ca6-4042-bbba-a72be7ae53c8\"]\)\>\"", ",", 
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{
       RowBox[{"3", "s"}], "+", "5"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"s", ",", 
       RowBox[{"-", "1"}], ",", "t"}], "}"}]}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.720780436907905*^9, 
  3.72078059428382*^9}},ExpressionUUID->"f7a07d19-f6a3-4ee4-a58a-\
c9dd91f728f6"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\!\\(\\*Cell[TextData[{\\n\\\"A[x(t)]=\\\",\\nCell[BoxData[\
FormBox[\\n  RowBox[{\\nSubsuperscriptBox[\\\"\[Integral]\\\", \
RowBox[{\\\"-\\\", \\\"1\\\"}], \\\"t\\\"], \\n    RowBox[{\\nSuperscriptBox[\
\\\"e\\\", RowBox[{\\n      RowBox[{\\\"3\\\", \\\"s\\\"}], \\\"+\\\", \
\\\"5\\\"}]], RowBox[{\\\"\[DifferentialD]\\\", \\\"s\\\"}]}]}], \\n   \
TraditionalForm]]],\\n\\\" = \\\"\\n}]]\\)\"\>", "\[InvisibleSpace]", 
   RowBox[{
    FractionBox["1", "3"], " ", 
    SuperscriptBox["\[ExponentialE]", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"3", "+", 
        RowBox[{"3", " ", "t"}]}]]}], ")"}]}]}],
  SequenceForm[
  "\!\(\*Cell[TextData[{\n\"A[x(t)]=\",\nCell[BoxData[FormBox[\n  RowBox[{\n\
SubsuperscriptBox[\"\[Integral]\", RowBox[{\"-\", \"1\"}], \"t\"], \n    \
RowBox[{\nSuperscriptBox[\"e\", RowBox[{\n      RowBox[{\"3\", \"s\"}], \
\"+\", \"5\"}]], RowBox[{\"\[DifferentialD]\", \"s\"}]}]}], \n   \
TraditionalForm]]],\n\" = \"\n}]]\)", Rational[1, 3] 
   E^2 (-1 + E^(3 + 3 $CellContext`t))],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.720780595581935*^9},ExpressionUUID->"c7b77950-97d8-46d0-9f19-\
d6cb5d5868f2"]
}, {2}]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435 \:043f\:0443\:043d\:043a\
\:0442\:0430 b", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 3.716642603163493*^9}, {
  3.719563702090982*^9, 3.719563703767712*^9}, {3.719564239235032*^9, 
  3.719564241607011*^9}, {3.7207806498313847`*^9, 
  3.720780650711033*^9}},ExpressionUUID->"64cc1244-4291-4b3c-aeee-\
f5ed6f0dc0e1"],

Cell[TextData[{
 "\:041f\:0440\:043e\:0432\:0435\:0440\:0438\:043c \:0432\:044b\:043f\:043e\
\:043b\:043d\:0435\:043d\:0438\:0435 \:0443\:0442\:0432\:0435\:0440\:0436\
\:0434\:0435\:043d\:0438\:044f ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"A", "[", 
     RowBox[{
      SubscriptBox["x", "1"], "+", 
      SubscriptBox["x", "2"]}], "]"}], "=", 
    RowBox[{
     RowBox[{"A", "[", 
      SubscriptBox["x", "1"], "]"}], "+", 
     RowBox[{"A", "[", 
      SubscriptBox["x", "2"], "]"}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "5516ba20-7036-4482-a7e0-686bfd1fb8e2"],
 ":"
}], "Text",
 CellChangeTimes->{{3.719564369832242*^9, 3.719564384408202*^9}, {
  3.7195645888246603`*^9, 3.719564593241581*^9}, {3.7207798970839977`*^9, 
  3.720779991302093*^9}},ExpressionUUID->"49cd4b36-c8e4-4b3b-a6b6-\
1bc6666a4957"],

Cell[TextData[Cell[BoxData[{
 FormBox[
  RowBox[{
   RowBox[{"A", "[", 
    RowBox[{
     SubscriptBox["x", "1"], "+", 
     SubscriptBox["x", "2"]}], "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["t", "2"], "-", "1"}], ")"}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SubscriptBox["x", "1"], "+", 
        SubscriptBox["x", "2"]}], ")"}], "''"}], 
     RowBox[{"(", "t", ")"}]}], "+", 
    RowBox[{"2", 
     RowBox[{
      RowBox[{"t", "(", 
       RowBox[{
        SubscriptBox["x", "1"], "+", 
        SubscriptBox["x", "2"]}], ")"}], "'"}], 
     RowBox[{"(", "t", ")"}]}]}]}], TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["t", "2"], "-", "1"}], ")"}], 
      RowBox[{
       SubscriptBox["x", "1"], "''"}], 
      RowBox[{"(", "t", ")"}]}], "+", 
     RowBox[{"2", 
      RowBox[{
       SubscriptBox["tx", "1"], "'"}], 
      RowBox[{"(", "t", ")"}]}], "+", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["t", "2"], "-", "1"}], ")"}], 
      RowBox[{
       SubscriptBox["x", "2"], "''"}], 
      RowBox[{"(", "t", ")"}]}], "+", 
     RowBox[{"2", 
      RowBox[{
       SubscriptBox["tx", "2"], "'"}], 
      RowBox[{"(", "t", ")"}]}]}], "=", 
    RowBox[{
     RowBox[{"A", "[", 
      SubscriptBox["x", "1"], "]"}], "+", 
     RowBox[{"A", "[", 
      SubscriptBox["x", "2"], "]"}]}]}]}], TraditionalForm]}],
 FormatType->
  "TraditionalForm",ExpressionUUID->"be564089-751e-454c-826e-912d801042dc"]], \
"Text",
 CellChangeTimes->{{3.720779992861618*^9, 3.720780017854251*^9}, {
  3.7207800522710857`*^9, 3.720780128583242*^9}, {3.720780168024652*^9, 
  3.7207801864308567`*^9}, {3.720780902930622*^9, 3.720781061497344*^9}, {
  3.7207814295322313`*^9, 
  3.720781429532462*^9}},ExpressionUUID->"62169a5e-dbdd-4c61-be49-\
ab92f9e2585d"],

Cell[TextData[{
 "\:0418\:0437 \:043f\:0440\:0430\:0432\:0438\:043b \:0432\:0437\:044f\:0442\
\:0438\:044f \:043f\:0440\:043e\:0438\:0437\:0432\:043e\:0434\:043d\:044b\
\:0445 \:0441\:043b\:0435\:0434\:0443\:0435\:0442, \:0447\:0442\:043e \:0434\
\:0430\:043d\:043d\:043e\:0435 \:043e\:0442\:043d\:043e\:0448\:0435\:043d\
\:0438\:0435 \:0432\:044b\:043f\:043e\:043b\:043d\:044f\:0435\:0442\:0441\
\:044f.\n\:041f\:0440\:043e\:0432\:0435\:0440\:0438\:043c \:0443\:0442\:0432\
\:0435\:0440\:0436\:0434\:0435\:043d\:0438\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"A", "[", "\[Alpha]x", "]"}], "=", 
    RowBox[{"\[Alpha]A", "[", "x", "]"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "e26c6be3-7651-4751-824e-7f6d01f69fd5"],
 ":"
}], "Text",
 CellChangeTimes->{{3.720780244476317*^9, 3.7207803419813747`*^9}, {
  3.720781177518795*^9, 
  3.720781262007518*^9}},ExpressionUUID->"c3a4cbe1-806e-4568-b2f4-\
3279d3a1b289"],

Cell[TextData[Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{"A", "[", "\[Alpha]x", "]"}], " ", "=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["t", "2"], "-", "1"}], ")"}], 
      RowBox[{
       RowBox[{"(", "\[Alpha]x", ")"}], "''"}], 
      RowBox[{"(", "t", ")"}]}], "+", 
     RowBox[{"2", 
      RowBox[{"t", "(", "\[Alpha]", ")"}], 
      RowBox[{"x", "'"}], 
      RowBox[{"(", "t", ")"}]}]}], "=", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"\[Alpha]", "(", 
        RowBox[{
         SuperscriptBox["t", "2"], "-", "1"}], ")"}], 
       RowBox[{"x", "''"}], 
       RowBox[{"(", "t", ")"}]}], "+", 
      RowBox[{
       RowBox[{"\[Alpha]2tx", "'"}], 
       RowBox[{"(", "t", ")"}]}]}], "=", 
     RowBox[{"\[Alpha]A", "[", "x", "]"}]}]}]}], TraditionalForm]],
 FormatType->
  "TraditionalForm",ExpressionUUID->"f0874573-6ca8-4063-b8bc-106eacfdd8b3"]], \
"Text",
 CellChangeTimes->{{3.720781266534562*^9, 3.720781266920515*^9}, {
  3.720781355052095*^9, 
  3.720781487304583*^9}},ExpressionUUID->"c052c137-8867-4c6f-8608-\
ec4382d0db73"],

Cell["\<\
\:0412\:0442\:043e\:0440\:043e\:0435 \:0443\:0441\:043b\:043e\:0432\:0438\
\:0435 \:0442\:0430\:043a\:0436\:0435 \:0432\:044b\:043f\:043e\:043b\:043d\
\:044f\:0435\:0442\:0441\:044f, \:0441\:043b\:0435\:0434\:043e\:0432\:0430\
\:0442\:0435\:043b\:044c\:043d\:043e \:043e\:043f\:0435\:0440\:0430\:0442\
\:043e\:0440 \:043b\:0438\:043d\:0435\:0439\:043d\:044b\:0439.\
\>", "Text",
 CellChangeTimes->{{3.7207814947862387`*^9, 3.720781514883417*^9}, {
  3.7207817879966497`*^9, 
  3.720781788751956*^9}},ExpressionUUID->"91271a74-aac3-4fa8-a87e-\
aaca1f76c1a2"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"x", "[", "t_", "]"}], "=", 
   RowBox[{
    SuperscriptBox["t", "3"], "-", 
    RowBox[{"3", 
     SuperscriptBox["t", "2"]}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ax", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["t", "2"], "-", "1"}], ")"}], "*", 
     RowBox[{
      RowBox[{"x", "''"}], "[", "t", "]"}]}], "+", 
    RowBox[{"2", "t", "*", 
     RowBox[{
      RowBox[{"x", "'"}], "[", "t", "]"}]}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<\!\(\*Cell[TextData[{
\"A[x(t)]= \",
Cell[BoxData[FormBox[RowBox[{RowBox[{RowBox[{RowBox[{\"(\", RowBox[{
SuperscriptBox[\"t\", \"2\"], \"-\", \"1\"}], \")\"}], RowBox[{RowBox[{\"(\", \
RowBox[{
SuperscriptBox[\"t\", \"3\"], \"-\", RowBox[{\"3\", 
SuperscriptBox[\"t\", \"2\"]}]}], \")\"}], \"''\"}], RowBox[{\"(\", \"t\", \
\")\"}]}], \"+\", RowBox[{\"2\", RowBox[{RowBox[{\"t\", \"(\", RowBox[{
SuperscriptBox[\"t\", \"3\"], \"-\", RowBox[{\"3\", 
SuperscriptBox[\"t\", \"2\"]}]}], \")\"}], \"'\"}], RowBox[{\"(\", \"t\", \")\
\"}]}]}], \" \", \"=\", \" \"}], \
TraditionalForm]],ExpressionUUID->\"f18bed92-3421-4520-8ab7-4a93f79de961\"]
}],ExpressionUUID->\"dbd07790-8944-40f1-8531-8d52e043cd1f\"]\)\>\"", ",", 
   "ax", ",", " ", "\"\< = \>\"", ",", " ", 
   RowBox[{"Expand", "[", "ax", "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.720780436907905*^9, 3.72078059428382*^9}, {
  3.7207812358343067`*^9, 3.720781237878147*^9}, {3.72078152931737*^9, 
  3.720781529319084*^9}, {3.720781591049706*^9, 
  3.720781766634654*^9}},ExpressionUUID->"6cae6944-4549-46f2-bdf4-\
c38862ba7f48"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\!\\(\\*Cell[TextData[{\\n\\\"A[x(t)]= \
\\\",\\nCell[BoxData[FormBox[\\n  RowBox[{RowBox[{\\n    \
RowBox[{RowBox[{\\\"(\\\", \\n       RowBox[{\\nSuperscriptBox[\\\"t\\\", \
\\\"2\\\"], \\\"-\\\", \\\"1\\\"}], \\\")\\\"}], \\n      \
RowBox[{RowBox[{\\\"(\\\", \\n        RowBox[{\\nSuperscriptBox[\\\"t\\\", \\\
\"3\\\"], \\\"-\\\", \\n         RowBox[{\\\"3\\\", \\nSuperscriptBox[\\\"t\\\
\", \\\"2\\\"]}]}], \\\")\\\"}], \\\"''\\\"}],\\n       RowBox[{\\\"(\\\", \\\
\"t\\\", \\\")\\\"}]}], \\\"+\\\", \\n     RowBox[{\\\"2\\\", RowBox[{\\n     \
 RowBox[{\\\"t\\\", \\\"(\\\", \\n        \
RowBox[{\\nSuperscriptBox[\\\"t\\\", \\\"3\\\"], \\\"-\\\", \\n         \
RowBox[{\\\"3\\\", \\nSuperscriptBox[\\\"t\\\", \\\"2\\\"]}]}], \\\")\\\"}], \
\\\"'\\\"}], \\n      RowBox[{\\\"(\\\", \\\"t\\\", \\\")\\\"}]}]}], \\\" \
\\\", \\\"=\\\", \\\" \\\"}], \\n   TraditionalForm]]]\\n}]]\\)\"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "6"}], "+", 
       RowBox[{"6", " ", "t"}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       SuperscriptBox["t", "2"]}], ")"}]}], "+", 
    RowBox[{"2", " ", "t", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "6"}], " ", "t"}], "+", 
       RowBox[{"3", " ", 
        SuperscriptBox["t", "2"]}]}], ")"}]}]}], 
   "\[InvisibleSpace]", "\<\" = \"\>", "\[InvisibleSpace]", 
   RowBox[{"6", "-", 
    RowBox[{"6", " ", "t"}], "-", 
    RowBox[{"18", " ", 
     SuperscriptBox["t", "2"]}], "+", 
    RowBox[{"12", " ", 
     SuperscriptBox["t", "3"]}]}]}],
  SequenceForm[
  "\!\(\*Cell[TextData[{\n\"A[x(t)]= \",\nCell[BoxData[FormBox[\n  \
RowBox[{RowBox[{\n    RowBox[{RowBox[{\"(\", \n       RowBox[{\n\
SuperscriptBox[\"t\", \"2\"], \"-\", \"1\"}], \")\"}], \n      \
RowBox[{RowBox[{\"(\", \n        RowBox[{\nSuperscriptBox[\"t\", \"3\"], \
\"-\", \n         RowBox[{\"3\", \nSuperscriptBox[\"t\", \"2\"]}]}], \")\"}], \
\"''\"}],\n       RowBox[{\"(\", \"t\", \")\"}]}], \"+\", \n     \
RowBox[{\"2\", RowBox[{\n      RowBox[{\"t\", \"(\", \n        RowBox[{\n\
SuperscriptBox[\"t\", \"3\"], \"-\", \n         RowBox[{\"3\", \n\
SuperscriptBox[\"t\", \"2\"]}]}], \")\"}], \"'\"}], \n      RowBox[{\"(\", \
\"t\", \")\"}]}]}], \" \", \"=\", \" \"}], \n   TraditionalForm]]]\n}]]\)", \
(-6 + 6 $CellContext`t) (-1 + $CellContext`t^2) + 
   2 $CellContext`t ((-6) $CellContext`t + 3 $CellContext`t^2), " = ", 6 - 
   6 $CellContext`t - 18 $CellContext`t^2 + 12 $CellContext`t^3],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.7207817669559517`*^9},ExpressionUUID->"9e0a062d-eb2d-4e95-9bf4-\
701e70d4cea4"]
}, {2}]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1054, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/36.pdf"},
Magnification:>1.25 Inherited,
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1276, 19, 118, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1881, 45, 187, 3, 84, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[2071, 50, 862, 20, 44, "Text",ExpressionUUID->"16862608-bc0d-41bc-abc3-86ba35c82cd4"],
Cell[2936, 72, 2020, 66, 82, "Text",ExpressionUUID->"0b17c59d-7944-4125-b3fa-b837a331a638"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4993, 143, 323, 5, 84, "Section",ExpressionUUID->"3b32b734-bf0a-4546-be02-6bb86c53665d"],
Cell[5319, 150, 855, 23, 44, "Text",ExpressionUUID->"ac9b598f-a538-4b43-9070-7bc651f96fe1"],
Cell[6177, 175, 1459, 45, 88, "Text",ExpressionUUID->"e62c69e3-ee42-47f8-bfd9-282136c1ba74"],
Cell[7639, 222, 1050, 16, 73, "Text",ExpressionUUID->"fa59c24b-16d1-4112-a7ba-0b4dc348677f"],
Cell[CellGroupData[{
Cell[8714, 242, 851, 21, 46, "Input",ExpressionUUID->"f7a07d19-f6a3-4ee4-a58a-c9dd91f728f6"],
Cell[9568, 265, 1283, 27, 50, "Print",ExpressionUUID->"c7b77950-97d8-46d0-9f19-d6cb5d5868f2"]
}, {2}]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10897, 298, 374, 6, 84, "Section",ExpressionUUID->"64cc1244-4291-4b3c-aeee-f5ed6f0dc0e1"],
Cell[11274, 306, 855, 23, 44, "Text",ExpressionUUID->"49cd4b36-c8e4-4b3b-a6b6-1bc6666a4957"],
Cell[12132, 331, 1958, 64, 81, "Text",ExpressionUUID->"62169a5e-dbdd-4c61-be49-ab92f9e2585d"],
Cell[14093, 397, 961, 20, 73, "Text",ExpressionUUID->"c3a4cbe1-806e-4568-b2f4-3279d3a1b289"],
Cell[15057, 419, 1119, 35, 47, "Text",ExpressionUUID->"c052c137-8867-4c6f-8608-ec4382d0db73"],
Cell[16179, 456, 567, 10, 44, "Text",ExpressionUUID->"91271a74-aac3-4fa8-a87e-aaca1f76c1a2"],
Cell[CellGroupData[{
Cell[16771, 470, 1674, 40, 140, "Input",ExpressionUUID->"6cae6944-4549-46f2-bdf4-c38862ba7f48"],
Cell[18448, 512, 2736, 57, 71, "Print",ExpressionUUID->"9e0a062d-eb2d-4e95-9bf4-701e70d4cea4"]
}, {2}]]
}, Open  ]]
}, Open  ]]
}
]
*)

