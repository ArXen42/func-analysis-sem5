(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     18945,        481]
NotebookOptionsPosition[     16725,        433]
NotebookOutlinePosition[     17264,        453]
CellTagsIndexPosition[     17221,        450]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 28, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, 3.717786296159808*^9, {
   3.71786504637932*^9, 3.7178650467360163`*^9}, {3.717919799320438*^9, 
   3.717919799615849*^9}, {3.71792363608844*^9, 3.717923636650264*^9}, 
   3.718468781522575*^9},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:041e\:043f\:0440\:0435\:0434\:0435\:043b\:0438\:0442\:044c, \:043f\:0440\
\:0438\:043d\:0430\:0434\:043b\:0435\:0436\:0438\:0442 \:043b\:0438 \:0431\
\:0435\:0441\:043a\:043e\:043d\:0435\:0447\:043d\:0430\:044f \:0447\:0438\
\:0441\:043b\:043e\:0432\:0430\:044f \:043f\:043e\:0441\:043b\:0435\:0434\
\:043e\:0432\:0430\:0442\:0435\:043b\:044c\:043d\:043e\:0441\:0442\:044c ",
 Cell[BoxData[
  FormBox["x", TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "fe5a5573-2c88-4775-ab6f-79b980a5633b"],
 " \:0448\:0430\:0440\:0443 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["B", "1"], "(", "0", ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "20c0d9e5-d21a-471e-bab2-f1712bcadd1c"],
 " \:0432 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\:0442\:0432\
\:0435 ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["l", "2"], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "78c8051e-df23-4a46-a9f5-1582a410619e"],
 "."
}], "Text",
 CellChangeTimes->{{3.718468798080266*^9, 3.7184688550655203`*^9}, {
  3.718468894378112*^9, 3.7184689009461*^9}, {3.718469060384262*^9, 
  3.718469128101501*^9}, {3.7184704535511217`*^9, 
  3.718470454371935*^9}},ExpressionUUID->"8abc1242-bfb1-427e-9107-\
46b11597f7df"],

Cell[BoxData[
 RowBox[{"x", " ", "=", " ", 
  FormBox[
   SuperscriptBox[
    SubscriptBox[
     RowBox[{"{", 
      FractionBox[
       RowBox[{
        SuperscriptBox["2", "k"], " ", 
        RowBox[{
         SuperscriptBox["tan", 
          RowBox[{"-", "1"}]], "(", 
         RowBox[{"k", "+", "1"}], ")"}]}], 
       SuperscriptBox["3", "k"]], "}"}], 
     RowBox[{"k", "=", "0"}]], "\[Infinity]"],
   TraditionalForm]}]], "Text",
 CellChangeTimes->{{3.718470455194831*^9, 
  3.718470457917418*^9}},ExpressionUUID->"a4902929-e82b-4f5f-b342-\
4c150886c80c"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435", "Section",
 CellChangeTimes->{{3.716646160199793*^9, 3.716646165043662*^9}, {
   3.716734600686346*^9, 3.716734608587257*^9}, {3.7178650570935907`*^9, 
   3.7178650582335978`*^9}, 
   3.717922522247588*^9},ExpressionUUID->"8c178dec-d468-439d-aef4-\
0f7d38b23ab3"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"x", "=", 
   FractionBox[
    RowBox[{
     SuperscriptBox["2", "k"], 
     RowBox[{"ArcTan", "[", 
      RowBox[{"k", "+", "1"}], "]"}]}], 
    SuperscriptBox["3", "k"]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<\:041d\:0435\:043e\:0431\:0445\:043e\:0434\:0438\:043c\:043e \:043f\
\:0440\:043e\:0432\:0435\:0440\:0438\:0442\:044c \:0443\:0441\:043b\:043e\
\:0432\:0438\:0435 \!\(\*Cell[TextData[Cell[BoxData[FormBox[RowBox[{\"||\", \
\"x\", 
SubscriptBox[\"||\", 
SuperscriptBox[\"l\", \"2\"]]}], \
TraditionalForm]],ExpressionUUID->\"805db55b-4dec-4a6f-b9a0-06de0c2c6acd\"]],\
ExpressionUUID->\"b2b3db07-6042-4544-8b87-521cf4a51c68\"]\) = \>\"", ",", 
   "\[IndentingNewLine]", 
   RowBox[{"TraditionalForm", "[", 
    RowBox[{
     RowBox[{"Sqrt", "[", 
      RowBox[{"Sum", "[", 
       RowBox[{
        SuperscriptBox["x", "2"], ",", 
        RowBox[{"{", 
         RowBox[{"k", ",", "0", ",", "Infinity"}], "}"}]}], "]"}], "]"}], "<",
      " ", "1"}], "]"}], ",", "\[IndentingNewLine]", " ", "\"\<.\>\""}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.718470015950487*^9, 3.7184700166729403`*^9}, {
  3.7184701445733547`*^9, 3.718470355302381*^9}, {3.7184713221857157`*^9, 
  3.718471353513193*^9}},ExpressionUUID->"b017f52e-82da-4e58-972f-\
1bfc4a72a4f3"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\:041d\:0435\:043e\:0431\:0445\:043e\:0434\:0438\:043c\:043e \
\:043f\:0440\:043e\:0432\:0435\:0440\:0438\:0442\:044c \:0443\:0441\:043b\
\:043e\:0432\:0438\:0435 \\!\\(\\*Cell[TextData[Cell[BoxData[FormBox[\\n \
RowBox[{\\\"||\\\", \\\"x\\\", \\n\\n   SubscriptBox[\\\"||\\\", \
\\nSuperscriptBox[\\\"l\\\", \\\"2\\\"]]}], TraditionalForm]]]]]\\) = \"\>", 
   "\[InvisibleSpace]", 
   TagBox[
    FormBox[
     RowBox[{
      SqrtBox[
       RowBox[{
        UnderoverscriptBox["\[Sum]", 
         RowBox[{"k", "=", "0"}], "\[Infinity]"], 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           FractionBox["2", "3"], ")"}], 
          RowBox[{"2", " ", "k"}]], " ", 
         SuperscriptBox[
          RowBox[{
           SuperscriptBox["tan", 
            RowBox[{"-", "1"}]], "(", 
           RowBox[{"k", "+", "1"}], ")"}], "2"]}]}]], "<", "1"}],
     TraditionalForm],
    TraditionalForm,
    Editable->True], "\[InvisibleSpace]", "\<\".\"\>"}],
  SequenceForm[
  "\:041d\:0435\:043e\:0431\:0445\:043e\:0434\:0438\:043c\:043e \:043f\:0440\
\:043e\:0432\:0435\:0440\:0438\:0442\:044c \:0443\:0441\:043b\:043e\:0432\
\:0438\:0435 \!\(\*Cell[TextData[Cell[BoxData[FormBox[\n RowBox[{\"||\", \
\"x\", \n\n   SubscriptBox[\"||\", \nSuperscriptBox[\"l\", \"2\"]]}], \
TraditionalForm]]]]]\) = ", 
   TraditionalForm[
   Sum[Rational[2, 3]^(2 $CellContext`k) 
       ArcTan[1 + $CellContext`k]^2, {$CellContext`k, 0, 
        DirectedInfinity[1]}]^Rational[1, 2] < 1], "."],
  Editable->False]], "Print",
 CellChangeTimes->{{3.718470657681616*^9, 3.718470697878479*^9}, {
   3.7184708942114067`*^9, 3.718470910734748*^9}, {3.718470962648838*^9, 
   3.7184710124020033`*^9}, {3.718471324526626*^9, 3.718471357480928*^9}, 
   3.718534028416265*^9},ExpressionUUID->"b737fa7d-55ed-49da-981c-\
9eca753433ca"]
}, {2}]],

Cell[TextData[{
 "\:041f\:043e\:0441\:0442\:0440\:043e\:0438\:043c \:0433\:0440\:0430\:0444\
\:0438\:043a \:044d\:043b\:0435\:043c\:0435\:043d\:0442\:043e\:0432 \:0441\
\:0443\:043c\:043c\:044b ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["x", "k"], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "1465253d-338f-4df4-9cf7-c6c7a6e14b34"],
 ", \:0447\:0442\:043e\:0431\:044b \:0443\:0431\:0435\:0434\:0438\:0442\:044c\
\:0441\:044f, \:0447\:0442\:043e \:043e\:043d\:0438 \:043d\:0435\:043e\:0442\
\:0440\:0438\:0446\:0430\:0442\:0435\:043b\:044c\:043d\:044b:"
}], "Text",
 CellChangeTimes->{{3.718534040433573*^9, 3.718534073144943*^9}, {
  3.718534117789116*^9, 3.71853412913074*^9}, {3.7185342141979713`*^9, 
  3.718534221874587*^9}, {3.7185344279679413`*^9, 
  3.71853442966448*^9}},ExpressionUUID->"98ebaa13-d4ce-4e01-9a28-\
e9bbd8cbb04e"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"$FrontEndSession", ",", 
    RowBox[{"ShowCellLabel", "\[Rule]", "False"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"DiscretePlot", "[", 
  RowBox[{"x", ",", 
   RowBox[{"{", 
    RowBox[{"k", ",", "1", ",", "20"}], "}"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7185342057904654`*^9, 3.718534231548174*^9}, {
  3.718534405158779*^9, 3.7185344059396*^9}},
 NumberMarks->False,ExpressionUUID->"27ef4184-5e0b-46fd-ae6c-f030fb825622"],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.011000000000000001`], 
    AbsoluteThickness[1.6], {
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.011000000000000001`], AbsoluteThickness[1.6], Opacity[0.2], LineBox[{},
       VertexColors->None]}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.011000000000000001`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{1., 0.7380991451960602}, {1., 0}}, {{2., 
         0.5551314543992242}, {2., 0}}, {{3., 0.39283486330904666`}, {
         3., 0}}, {{4., 0.27128904038420065`}, {4., 0}}, {{5., 
         0.18510586329287507`}, {5., 0}}, {{6., 0.12544520359424813`}, {
         6., 0}}, {{7., 0.08465683151703762}, {7., 0}}, {{8., 
         0.056972353458158244`}, {8., 0}}, {{9., 0.038267406860921206`}, {
         9., 0}}, {{10., 0.025667830346735948`}, {10., 0}}, {{11., 
         0.017198810221840735`}, {11., 0}}, {{12., 0.011514964197175013`}, {
         12., 0}}, {{13., 0.007704720284147375}, {13., 0}}, {{14., 
         0.0051527146052806295`}, {14., 0}}, {{15., 0.0034446187762223034`}, {
         15., 0}}, {{16., 0.0023019891915886633`}, {16., 0}}, {{17., 
         0.0015379655054802283`}, {17., 0}}, {{18., 0.00102728304126523}, {
         18., 0}}, {{19., 0.0006860393292264044}, {19., 0}}, {{20., 
         0.00045807387133126027`}, {20., 0}}},
       VertexColors->None]}}}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.011000000000000001`], 
    AbsoluteThickness[1.6], {}, PointBox[CompressedData["
1:eJxTTMoPSmViYGAQAWIQDQEf7FmDrSSb5j23hwo4HGcx4Vp8+CGUz+HwknXl
NROVm1C+gMOx5bxbz8RchPJFHD4yT2rr2XgcypdwsPctWDyN7wCUL+MgsWL/
J8E1W6F8BYetLIe+VmmvhfKVHP5e/SB5cvpiKF/FwUNoxvtVHrOgfDWHEr/d
cyfMmQjlazj836cYqjG1HcrXcmB9EST1p68eytdxiH5Q6M0sXQrl6znEnNg2
/7F5DpRv4GAVnMo5/3YSlG/osNtU8vFGo0go38ih+obFkT1XAqB8Y4cgnzO2
r6rcoHwTh787X3ibsNrZAwCJKVYS
     "]], {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{1, 0},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "MessagesHead" -> DiscretePlot, "AxisPadding" -> Scaled[0.02], 
    "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "DefaultPlotStyle" -> {
      Directive[
       RGBColor[0.368417, 0.506779, 0.709798], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.880722, 0.611041, 0.142051], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.560181, 0.691569, 0.194885], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.922526, 0.385626, 0.209179], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.528488, 0.470624, 0.701351], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.772079, 0.431554, 0.102387], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.363898, 0.618501, 0.782349], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[1, 0.75, 0], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.647624, 0.37816, 0.614037], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.571589, 0.586483, 0.], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.915, 0.3325, 0.2125], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.40082222609352647`, 0.5220066643438841, 0.85], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.9728288904374106, 0.621644452187053, 0.07336199581899142], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.736782672705901, 0.358, 0.5030266573755369], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.28026441037696703`, 0.715, 0.4292089322474965], 
       AbsoluteThickness[1.6]]}, "DomainPadding" -> Scaled[0.02], 
    "PointSizeFunction" -> "SmallPointSize", "RangePadding" -> Scaled[0.05]},
  PlotRange->{{1, 20}, {0, 0.7380991451960602}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.02]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.7185341810700397`*^9, 3.718534231964616*^9}, 
   3.718534407284124*^9},ExpressionUUID->"d12c422a-0cd0-4160-9eb7-\
82264d132586"]
}, {2}]],

Cell["\:0412\:044b\:043f\:0438\:0448\:0435\:043c \:043f\:0435\:0440\:0432\
\:044b\:0435 \:0434\:0432\:0430 \:0441\:043b\:0430\:0433\:0430\:0435\:043c\
\:044b\:0445 \:0441\:0443\:043c\:043c\:044b \:043f\:043e\:0434 \:043a\:043e\
\:0440\:043d\:0435\:043c:", "Text",
 CellChangeTimes->{{3.718470624676477*^9, 3.718470636102261*^9}, {
  3.71847070207833*^9, 3.7184707034533567`*^9}, {3.718470795955967*^9, 
  3.7184707990949306`*^9}, {3.718470924994082*^9, 
  3.718470932807982*^9}},ExpressionUUID->"0cdb3daa-c562-46ac-9aaa-\
e40550e70481"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"firstElements", "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    SuperscriptBox["x", "2"], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", "1"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.718470568618698*^9, 3.7184706166748657`*^9}, {
  3.718470735460334*^9, 
  3.718470788636238*^9}},ExpressionUUID->"fba552b4-ab9a-41c5-947d-\
3aa3d974fea0"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox[
    SuperscriptBox["\[Pi]", "2"], "16"], ",", 
   FractionBox[
    RowBox[{"4", " ", 
     SuperscriptBox[
      RowBox[{"ArcTan", "[", "2", "]"}], "2"]}], "9"]}], "}"}]], "Output",
 CellChangeTimes->{
  3.718470617153801*^9, 3.71847070436489*^9, {3.718470746714121*^9, 
   3.718470800332591*^9}, {3.718470912476056*^9, 3.718470934191782*^9}, 
   3.718534028561644*^9},ExpressionUUID->"ada84b36-67bb-4105-b826-\
e146e17af015"]
}, {2}]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<\:0418\:0445 \:0441\:0443\:043c\:043c\:0430 \:0440\:0430\:0432\:043d\
\:0430 \>\"", ",", " ", 
   RowBox[{"N", "[", 
    RowBox[{"Total", "[", "firstElements", "]"}], "]"}], ",", " ", 
   "\"\<.\>\""}], "]"}]], "Input",
 CellChangeTimes->{{3.718470706176896*^9, 3.7184707653505898`*^9}, {
  3.71847140504692*^9, 
  3.718471406591415*^9}},ExpressionUUID->"61afd189-7f81-4e90-b861-\
36908bd5fa4e"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\:0418\:0445 \:0441\:0443\:043c\:043c\:0430 \:0440\:0430\:0432\
\:043d\:0430 \"\>", "\[InvisibleSpace]", "1.1616406232072398`", 
   "\[InvisibleSpace]", "\<\".\"\>"}],
  SequenceForm[
  "\:0418\:0445 \:0441\:0443\:043c\:043c\:0430 \:0440\:0430\:0432\:043d\:0430 \
", 1.1616406232072398`, "."],
  Editable->False]], "Print",
 CellChangeTimes->{{3.718470756803276*^9, 3.718470801744977*^9}, 
   3.718470936743083*^9, 3.718471407796052*^9, 
   3.718534028569911*^9},ExpressionUUID->"6de846f1-aa15-4a8b-a14a-\
2f3dc1e5ca09"]
}, {2}]],

Cell[TextData[{
 "\:0422\:0430\:043a \:043a\:0430\:043a \:0432\:0441\:0435 \:0441\:043b\:0430\
\:0433\:0430\:0435\:043c\:044b\:0435 \:043d\:0435\:043e\:0442\:0440\:0438\
\:0446\:0430\:0442\:0435\:043b\:044c\:043d\:044b, \:0441\:0443\:043c\:043c\
\:0430 \:0447\:0438\:0441\:043b\:043e\:0432\:043e\:0433\:043e \:0440\:044f\
\:0434\:0430 \:0431\:0443\:0434\:0435\:0442 \:043d\:0435 \:043c\:0435\:043d\
\:044c\:0448\:0435 \:043f\:043e\:0441\:0447\:0438\:0442\:0430\:043d\:043d\
\:043e\:0439 \:0447\:0430\:0441\:0442\:0438\:0447\:043d\:043e\:0439, \:0437\
\:043d\:0430\:0447\:0438\:0442 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"||", "x", 
    SubscriptBox["||", 
     SuperscriptBox["l", "2"]]}], TraditionalForm]],ExpressionUUID->
  "a750a711-3424-405b-b123-3419b70dd174"],
 " > 1."
}], "Text",
 CellChangeTimes->{{3.718470804933845*^9, 3.718470871836892*^9}, {
  3.718471164517024*^9, 3.718471174780609*^9}, {3.718475080107139*^9, 
  3.718475085668803*^9}},ExpressionUUID->"bfdba240-78d2-4fc9-999a-\
7fb24cdc17e4"],

Cell[TextData[{
 "\:0412\:044b\:0432\:043e\:0434: ",
 Cell[BoxData[
  FormBox[
   RowBox[{"x", "\[NotElement]", 
    RowBox[{
     SubscriptBox["B", "1"], "(", "0", ")"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "fa5c9d48-ee56-43c9-90f5-4fdea4b2d7de"],
 "."
}], "Text",
 CellChangeTimes->{{3.7184711888909903`*^9, 
  3.71847120905302*^9}},ExpressionUUID->"9c46c8b1-3c93-4ef0-b575-\
7e1da197043a"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1918, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/print.pdf"},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 780, 12, 92, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1385, 38, 187, 3, 68, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[1575, 43, 1304, 30, 35, "Text",ExpressionUUID->"8abc1242-bfb1-427e-9107-46b11597f7df"],
Cell[2882, 75, 562, 18, 61, "Text",ExpressionUUID->"a4902929-e82b-4f5f-b342-4c150886c80c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3481, 98, 314, 5, 68, "Section",ExpressionUUID->"8c178dec-d468-439d-aef4-0f7d38b23ab3"],
Cell[CellGroupData[{
Cell[3820, 107, 1331, 33, 147, "Input",ExpressionUUID->"b017f52e-82da-4e58-972f-1bfc4a72a4f3"],
Cell[5154, 142, 1874, 43, 62, "Print",ExpressionUUID->"b737fa7d-55ed-49da-981c-9eca753433ca"]
}, {2}]],
Cell[7040, 188, 864, 17, 35, "Text",ExpressionUUID->"98ebaa13-d4ce-4e01-9a28-e9bbd8cbb04e"],
Cell[CellGroupData[{
Cell[7929, 209, 512, 12, 55, "Input",ExpressionUUID->"27ef4184-5e0b-46fd-ae6c-f030fb825622"],
Cell[8444, 223, 4303, 98, 244, "Output",ExpressionUUID->"d12c422a-0cd0-4160-9eb7-82264d132586"]
}, {2}]],
Cell[12759, 324, 536, 8, 35, "Text",ExpressionUUID->"0cdb3daa-c562-46ac-9aaa-e40550e70481"],
Cell[CellGroupData[{
Cell[13320, 336, 381, 10, 39, "Input",ExpressionUUID->"fba552b4-ab9a-41c5-947d-3aa3d974fea0"],
Cell[13704, 348, 482, 13, 58, "Output",ExpressionUUID->"ada84b36-67bb-4105-b826-e146e17af015"]
}, {2}]],
Cell[CellGroupData[{
Cell[14220, 366, 451, 11, 31, "Input",ExpressionUUID->"61afd189-7f81-4e90-b861-36908bd5fa4e"],
Cell[14674, 379, 569, 12, 25, "Print",ExpressionUUID->"6de846f1-aa15-4a8b-a14a-2f3dc1e5ca09"]
}, {2}]],
Cell[15255, 394, 1011, 20, 38, "Text",ExpressionUUID->"bfdba240-78d2-4fc9-999a-7fb24cdc17e4"],
Cell[16269, 416, 428, 13, 35, "Text",ExpressionUUID->"9c46c8b1-3c93-4ef0-b575-7e1da197043a"]
}, Open  ]]
}, Open  ]]
}
]
*)

