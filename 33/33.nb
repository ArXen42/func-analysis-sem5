(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16043,        437]
NotebookOptionsPosition[     13662,        384]
NotebookOutlinePosition[     14198,        404]
CellTagsIndexPosition[     14155,        401]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 33, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, 3.717786296159808*^9, {
   3.71786504637932*^9, 3.7178650467360163`*^9}, {3.717919799320438*^9, 
   3.717919799615849*^9}, {3.71792363608844*^9, 3.717923636650264*^9}, {
   3.718379681596424*^9, 3.718379682353087*^9}, {3.718381431981628*^9, 
   3.718381432046639*^9}, 
   3.718383638306239*^9},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:041f\:0440\:043e\:0432\:0435\:0440\:0438\:0442\:044c, \:044f\:0432\:043b\
\:044f\:044e\:0442\:0441\:044f \:043b\:0438 \:0444\:0443\:043d\:043a\:0446\
\:0438\:0438 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"x", "=", 
    RowBox[{"x", "(", "t", ")"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "f3bef726-8c51-48fe-b77b-0ca0a019acde"],
 " \:0438 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"y", "=", 
    RowBox[{"y", "(", "t", ")"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "6c9dddbb-cd45-4ce2-8d88-9c85fb3b4800"],
 " \:043e\:0440\:0442\:043e\:0433\:043e\:043d\:0430\:043b\:044c\:043d\:044b\
\:043c\:0438 \:0432 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\
\:0442\:0432\:0430\:0445 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{"a", ";", "b"}], ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "08f812ba-8644-4cf5-b689-38eaf588070d"],
 " \:0438 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{"c", ";", "d"}], ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "48a81526-2011-4469-ab5f-ae14692cbf40"],
 "."
}], "Text",
 CellChangeTimes->{{3.7183796057344112`*^9, 3.718379676356288*^9}, {
  3.718381463700431*^9, 3.718381464758333*^9}, {3.718383239896797*^9, 
  3.718383285576437*^9}},ExpressionUUID->"6d37a514-bbac-4875-b7ec-\
8b520ed57479"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"x", "[", "t_", "]"}], "=", 
   RowBox[{"1", "+", 
    RowBox[{"4", 
     RowBox[{"Sin", "[", 
      RowBox[{"2", "Pi", "*", "t"}], "]"}]}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<x = \>\"", ",", " ", 
   RowBox[{"x", "[", "t", "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7183796882191133`*^9, 3.718379689415036*^9}, {
  3.718379809698779*^9, 3.718379862297649*^9}, {3.718380156601553*^9, 
  3.718380158684504*^9}, {3.718381711487481*^9, 3.718381736422282*^9}, {
  3.718381795645853*^9, 3.718381824361827*^9}, {3.7183832487414293`*^9, 
  3.71838325031107*^9}, {3.718383296820023*^9, 3.718383302510483*^9}, {
  3.718383492000085*^9, 3.718383527303809*^9}, {3.718383647303801*^9, 
  3.71838364757115*^9}},ExpressionUUID->"1a45e8f0-ead0-4c4b-a1a9-\
098db961c574"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"x = \"\>", "\[InvisibleSpace]", 
   RowBox[{"1", "+", 
    RowBox[{"4", " ", 
     RowBox[{"Sin", "[", 
      RowBox[{"2", " ", "\[Pi]", " ", "t"}], "]"}]}]}]}],
  SequenceForm["x = ", 1 + 4 Sin[2 Pi $CellContext`t]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.7183798592081223`*^9, 3.7183801628476057`*^9, 3.7183817373085938`*^9, {
   3.7183817973175077`*^9, 3.7183818261592093`*^9}, {3.718383523476329*^9, 
   3.718383528047222*^9}, {3.718383641149218*^9, 
   3.7183836478950367`*^9}},ExpressionUUID->"1e10ab00-5734-47d4-a715-\
6182fb487ad6"]
}, {2}]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"y", "[", "t_", "]"}], "=", 
   RowBox[{
    RowBox[{"Sin", "[", 
     RowBox[{"Pi", " ", "t"}], "]"}], " ", "-", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"Pi", " ", "t"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<y = \>\"", ",", " ", 
   RowBox[{"y", "[", "t", "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7183798676266413`*^9, 3.718379897830083*^9}, {
   3.7183811698735228`*^9, 3.718381174590315*^9}, 3.718381271620243*^9, {
   3.718381743982143*^9, 3.718381792246608*^9}, 3.718381831808731*^9, {
   3.718382079035705*^9, 3.718382086230236*^9}, {3.718383659174039*^9, 
   3.718383687626711*^9}},ExpressionUUID->"d5af08ab-8d00-4963-af3f-\
773d637f30e2"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"y = \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     RowBox[{"Cos", "[", 
      RowBox[{"\[Pi]", " ", "t"}], "]"}]}], "+", 
    RowBox[{"Sin", "[", 
     RowBox[{"\[Pi]", " ", "t"}], "]"}]}]}],
  SequenceForm["y = ", -Cos[Pi $CellContext`t] + Sin[Pi $CellContext`t]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.718379901375916*^9, 3.718380163844893*^9, 3.718381174882799*^9, 
   3.718381272046136*^9, {3.718381775332274*^9, 3.718381798708577*^9}, 
   3.7183818327420177`*^9, {3.7183820813600073`*^9, 3.718382086587632*^9}, {
   3.718383680436508*^9, 
   3.7183836883261127`*^9}},ExpressionUUID->"b28916b3-9177-494b-bad7-\
89513a3fe753"]
}, {2}]],

Cell[TextData[Cell[BoxData[{
 FormBox[
  RowBox[{
   RowBox[{"(", 
    RowBox[{"a", ";", "b"}], ")"}], "=", 
   RowBox[{"(", 
    RowBox[{"0", ";", "1"}], ")"}]}], TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{
   RowBox[{"(", 
    RowBox[{"c", ";", "d"}], ")"}], "=", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], ",", "1"}], ")"}]}], TraditionalForm]}],
 FormatType->
  "TraditionalForm",ExpressionUUID->"66d41b4f-bd01-4553-9864-4c95eaba1b0a"]], \
"Text",
 CellChangeTimes->{{3.718383847731143*^9, 
  3.718383899277461*^9}},ExpressionUUID->"10ed5533-1e4f-48cb-b27d-\
f57fb89a9f83"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435", "Section",
 CellChangeTimes->{{3.716646160199793*^9, 3.716646165043662*^9}, {
   3.716734600686346*^9, 3.716734608587257*^9}, {3.7178650570935907`*^9, 
   3.7178650582335978`*^9}, 
   3.717922522247588*^9},ExpressionUUID->"8c178dec-d468-439d-aef4-\
0f7d38b23ab3"],

Cell["\<\
\:0414\:043b\:044f \:043f\:0440\:043e\:0432\:0435\:0440\:043a\:0438 \:043e\
\:0440\:0442\:043e\:0433\:043e\:043d\:0430\:043b\:044c\:043d\:043e\:0441\:0442\
\:0438 \:0444\:0443\:043d\:043a\:0446\:0438\:0439 x \:0438 y, \:043d\:0443\
\:0436\:043d\:043e \:043d\:0430\:0439\:0442\:0438 \:0438\:0445 \:0441\:043a\
\:0430\:043b\:044f\:0440\:043d\:043e\:0435 \:043f\:0440\:043e\:0438\:0437\
\:0432\:0435\:0434\:0435\:043d\:0438\:0435 \:0432 \:0438\:0441\:0441\:043b\
\:0435\:0434\:0443\:0435\:043c\:044b\:0445 \:043f\:0440\:043e\:0441\:0442\
\:0440\:0430\:043d\:0441\:0442\:0432\:0430\:0445 \:0438 \:043f\:0440\:043e\
\:0432\:0435\:0440\:0438\:0442\:044c \:043d\:0430 \:0440\:0430\:0432\:0435\
\:043d\:0441\:0442\:0432\:043e \:0441 \:043d\:0443\:043b\:0435\:043c.\
\>", "Text",
 CellChangeTimes->{{3.718383760850539*^9, 
  3.718383799731308*^9}},ExpressionUUID->"8fb79014-9709-4142-9ea1-\
31b8d3df57c9"],

Cell[CellGroupData[{

Cell[TextData[{
 "\:0412 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\:0442\:0432\
\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{"0", ";", "1"}], ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "a6f611b0-1a73-4246-9b0a-1eea78aa9a3f"]
}], "Subsection",
 CellChangeTimes->{{3.718383805288155*^9, 3.718383833573412*^9}, {
  3.718383904460857*^9, 
  3.718383908941547*^9}},ExpressionUUID->"f5bdfcd9-3179-42bb-933d-\
871ac166a5c0"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<(x,y) = \!\(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(1\)]\)x(t) \
y(t)\[DifferentialD]t = \>\"", ",", " ", 
   RowBox[{
    SubsuperscriptBox["\[Integral]", "0", "1"], 
    RowBox[{
     RowBox[{"x", "[", "t", "]"}], " ", 
     RowBox[{"y", "[", "t", "]"}], 
     RowBox[{"\[DifferentialD]", "t"}]}]}]}], "]"}]], "Input",
 CellChangeTimes->{{3.718384046992872*^9, 
  3.71838412622577*^9}},ExpressionUUID->"5e56acc9-1d1b-415c-8e7b-\
ea66016e63ff"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"(x,y) = \\!\\(\\*SubsuperscriptBox[\\(\[Integral]\\), \\(0\\), \
\\(1\\)]\\)x(t) y(t)\[DifferentialD]t = \"\>", "\[InvisibleSpace]", 
   RowBox[{"-", 
    FractionBox["10", 
     RowBox[{"3", " ", "\[Pi]"}]]}]}],
  SequenceForm[
  "(x,y) = \!\(\*SubsuperscriptBox[\(\[Integral]\), \(0\), \(1\)]\)x(t) y(t)\
\[DifferentialD]t = ", Rational[-10, 3]/Pi],
  Editable->False]], "Print",
 GeneratedCell->False,
 CellAutoOverwrite->False,
 CellChangeTimes->{{3.71838412064703*^9, 3.718384126832192*^9}, {
  3.718384170867133*^9, 
  3.7183841736822233`*^9}},ExpressionUUID->"f84670e6-49bb-493c-81c8-\
261144e26807"]
}, {2}]],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"(", 
     RowBox[{"x", ",", "y"}], ")"}], " ", "\[NotEqual]", "0"}], 
   TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "a87421f9-05cb-40a1-af3a-c2378bfdb6b0"],
 ", \:0441\:043b\:0435\:0434\:043e\:0432\:0430\:0442\:0435\:043b\:044c\:043d\
\:043e \:0444\:0443\:043d\:043a\:0446\:0438\:0438 x \:0438 y ",
 StyleBox["\:043d\:0435 \:044f\:0432\:043b\:044f\:044e\:0442\:0441\:044f",
  FontWeight->"Bold"],
 " \:043e\:0440\:0442\:043e\:0433\:043e\:043d\:0430\:043b\:044c\:043d\:044b\
\:043c\:0438 \:0432 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\
\:0442\:0432\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{"0", ";", "1"}], ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "e8c3b105-6905-4e7e-af9a-8e62ebcb2be1"],
 "."
}], "Text",
 CellChangeTimes->{{3.71838418391676*^9, 
  3.718384228827429*^9}},ExpressionUUID->"41845097-eb6c-4837-b094-\
3a2a852f0445"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "\:0412 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\:0442\:0432\
\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{
     RowBox[{"-", "1"}], ";", "1"}], ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "b298b61d-f0d4-4812-960e-1069b8e597f5"]
}], "Subsection",
 CellChangeTimes->{{3.718383805288155*^9, 3.718383833573412*^9}, {
  3.718383904460857*^9, 3.718383908941547*^9}, {3.7183842423406363`*^9, 
  3.718384242820772*^9}},ExpressionUUID->"7524fcd8-ebb8-4db8-af3d-\
82c9f9985b22"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<(x,y) = \!\(\*SubsuperscriptBox[\(\[Integral]\), \(-1\), \(1\)]\)x(t) \
y(t)\[DifferentialD]t = \>\"", ",", " ", 
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "1"}], "1"], 
    RowBox[{
     RowBox[{"x", "[", "t", "]"}], " ", 
     RowBox[{"y", "[", "t", "]"}], 
     RowBox[{"\[DifferentialD]", "t"}]}]}]}], "]"}]], "Input",
 CellChangeTimes->{{3.718384046992872*^9, 3.71838412622577*^9}, {
  3.718384249993325*^9, 3.718384250351248*^9}, {3.718384311525453*^9, 
  3.718384311646553*^9}},ExpressionUUID->"3917e566-527a-465d-814e-\
f7f3f252c608"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"(x,y) = \\!\\(\\*SubsuperscriptBox[\\(\[Integral]\\), \
\\(-1\\), \\(1\\)]\\)x(t) y(t)\[DifferentialD]t = \"\>", "\[InvisibleSpace]", 
   "0"}],
  SequenceForm[
  "(x,y) = \!\(\*SubsuperscriptBox[\(\[Integral]\), \(-1\), \(1\)]\)x(t) y(t)\
\[DifferentialD]t = ", 0],
  Editable->False]], "Print",
 CellChangeTimes->{{3.718384261752083*^9, 3.718384263276003*^9}, 
   3.718384313579864*^9},ExpressionUUID->"e1028310-bd84-461a-8759-\
7998d2571bd2"]
}, {2}]],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"(", 
     RowBox[{"x", ",", "y"}], ")"}], " ", "=", "0"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "3641f1b7-dc9f-4c01-8eea-e410d3aeafd1"],
 ", \:0441\:043b\:0435\:0434\:043e\:0432\:0430\:0442\:0435\:043b\:044c\:043d\
\:043e \:0444\:0443\:043d\:043a\:0446\:0438\:0438 x \:0438 y ",
 StyleBox["\:044f\:0432\:043b\:044f\:044e\:0442\:0441\:044f",
  FontWeight->"Bold"],
 " \:043e\:0440\:0442\:043e\:0433\:043e\:043d\:0430\:043b\:044c\:043d\:044b\
\:043c\:0438 \:0432 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\
\:0442\:0432\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{
     RowBox[{"-", "1"}], ";", "1"}], ")"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "96591f67-9329-4eb8-ad22-9cdb73ea276b"],
 "."
}], "Text",
 CellChangeTimes->{{3.71838418391676*^9, 3.718384228827429*^9}, {
  3.718384268816428*^9, 
  3.7183842768303022`*^9}},ExpressionUUID->"d562bb1d-0f85-4602-a6d9-\
8e9825670529"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1918, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/33.pdf"},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 880, 14, 92, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1485, 40, 187, 3, 68, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[1675, 45, 1464, 40, 35, "Text",ExpressionUUID->"6d37a514-bbac-4875-b7ec-8b520ed57479"],
Cell[CellGroupData[{
Cell[3164, 89, 850, 19, 55, "Input",ExpressionUUID->"1a45e8f0-ead0-4c4b-a1a9-098db961c574"],
Cell[4017, 110, 604, 14, 25, "Print",ExpressionUUID->"1e10ab00-5734-47d4-a715-6182fb487ad6"]
}, {2}]],
Cell[CellGroupData[{
Cell[4655, 129, 749, 17, 55, "Input",ExpressionUUID->"d5af08ab-8d00-4963-af3f-773d637f30e2"],
Cell[5407, 148, 711, 17, 25, "Print",ExpressionUUID->"b28916b3-9177-494b-bad7-89513a3fe753"]
}, {2}]],
Cell[6130, 168, 609, 19, 61, "Text",ExpressionUUID->"10ed5533-1e4f-48cb-b27d-f57fb89a9f83"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6776, 192, 314, 5, 68, "Section",ExpressionUUID->"8c178dec-d468-439d-aef4-0f7d38b23ab3"],
Cell[7093, 199, 906, 14, 58, "Text",ExpressionUUID->"8fb79014-9709-4142-9ea1-31b8d3df57c9"],
Cell[CellGroupData[{
Cell[8024, 217, 515, 14, 55, "Subsection",ExpressionUUID->"f5bdfcd9-3179-42bb-933d-871ac166a5c0"],
Cell[CellGroupData[{
Cell[8564, 235, 507, 13, 49, "Input",ExpressionUUID->"5e56acc9-1d1b-415c-8e7b-ea66016e63ff"],
Cell[9074, 250, 656, 16, 43, "Print",ExpressionUUID->"f84670e6-49bb-493c-81c8-261144e26807"]
}, {2}]],
Cell[9742, 269, 1028, 27, 35, "Text",ExpressionUUID->"41845097-eb6c-4837-b094-3a2a852f0445"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10807, 301, 584, 15, 55, "Subsection",ExpressionUUID->"7524fcd8-ebb8-4db8-af3d-82c9f9985b22"],
Cell[CellGroupData[{
Cell[11416, 320, 624, 15, 49, "Input",ExpressionUUID->"3917e566-527a-465d-814e-f7f3f252c608"],
Cell[12043, 337, 494, 11, 40, "Print",ExpressionUUID->"e1028310-bd84-461a-8759-7998d2571bd2"]
}, {2}]],
Cell[12549, 351, 1073, 28, 35, "Text",ExpressionUUID->"d562bb1d-0f85-4602-a6d9-8e9825670529"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

