(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     14812,        410]
NotebookOptionsPosition[     12305,        354]
NotebookOutlinePosition[     12844,        374]
CellTagsIndexPosition[     12801,        371]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 32, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, 3.717786296159808*^9, {
   3.71786504637932*^9, 3.7178650467360163`*^9}, {3.717919799320438*^9, 
   3.717919799615849*^9}, {3.71792363608844*^9, 3.717923636650264*^9}, {
   3.718379681596424*^9, 3.718379682353087*^9}, {3.718381431981628*^9, 
   3.718381432046639*^9}},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:0414\:0430\:043d\:0430 \:043e\:0440\:0442\:043e\:043d\:043e\:0440\:043c\
\:0438\:0440\:043e\:0432\:0430\:043d\:043d\:0430\:044f \:0441\:0438\:0441\
\:0442\:0435\:043c\:0430 ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    SuperscriptBox[
     RowBox[{"{", 
      SubscriptBox["e", "k"], "}"}], "\[Infinity]"], 
    RowBox[{"k", "=", "1"}]], TraditionalForm]],ExpressionUUID->
  "59215fe4-8c8a-418f-90f2-dd0d469ed267"],
 " \:0432 \:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\:0442\:0432\
\:0435 \:0441\:043e \:0441\:043a\:0430\:043b\:044f\:0440\:043d\:044b\:043c \
\:043f\:0440\:043e\:0438\:0437\:0432\:0435\:0434\:0435\:043d\:0438\:0435\:043c\
. \:041d\:0430\:0439\:0442\:0438 \:0443\:0433\:043e\:043b \:043c\:0435\:0436\
\:0434\:0443 \:044d\:043b\:0435\:043c\:0435\:043d\:0442\:0430\:043c\:0438 x \
\:0438 y."
}], "Text",
 CellChangeTimes->{{3.7183796057344112`*^9, 3.718379676356288*^9}, {
  3.718381463700431*^9, 
  3.718381464758333*^9}},ExpressionUUID->"6d37a514-bbac-4875-b7ec-\
8b520ed57479"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"x", "=", 
   RowBox[{
    RowBox[{
     SubscriptBox["e", "1"], "/", "3"}], "+", 
    RowBox[{"2", 
     RowBox[{
      SubscriptBox["e", "8"], "/", "5"}]}], "+", 
    RowBox[{
     SubscriptBox["e", "7"], "/", "2"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<x = \>\"", ",", " ", "x"}], "]"}]}], "Input",
 CellChangeTimes->{{3.7183796882191133`*^9, 3.718379689415036*^9}, {
  3.718379809698779*^9, 3.718379862297649*^9}, {3.718380156601553*^9, 
  3.718380158684504*^9}, {3.718381711487481*^9, 3.718381736422282*^9}, {
  3.718381795645853*^9, 
  3.718381824361827*^9}},ExpressionUUID->"1a45e8f0-ead0-4c4b-a1a9-\
098db961c574"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"x = \"\>", "\[InvisibleSpace]", 
   RowBox[{
    FractionBox[
     SubscriptBox["e", "1"], "3"], "+", 
    FractionBox[
     SubscriptBox["e", "7"], "2"], "+", 
    FractionBox[
     RowBox[{"2", " ", 
      SubscriptBox["e", "8"]}], "5"]}]}],
  SequenceForm[
  "x = ", Rational[1, 3] Subscript[$CellContext`e, 1] + 
   Rational[1, 2] Subscript[$CellContext`e, 7] + 
   Rational[2, 5] Subscript[$CellContext`e, 8]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.7183798592081223`*^9, 3.7183801628476057`*^9, 3.7183817373085938`*^9, {
   3.7183817973175077`*^9, 
   3.7183818261592093`*^9}},ExpressionUUID->"e0110ac2-0d05-4548-92fe-\
48a0b504e6e2"]
}, {2}]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"y", "=", 
   FractionBox[
    RowBox[{"(", 
     RowBox[{
      SubscriptBox["e", "9"], "-", 
      RowBox[{"5", 
       SubscriptBox["e", "8"]}], "+", 
      SubscriptBox["e", "7"]}], ")"}], "2"]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<y = \>\"", ",", " ", 
   RowBox[{"Expand", "[", "y", "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7183798676266413`*^9, 3.718379897830083*^9}, {
   3.7183811698735228`*^9, 3.718381174590315*^9}, 3.718381271620243*^9, {
   3.718381743982143*^9, 3.718381792246608*^9}, 3.718381831808731*^9, {
   3.718382079035705*^9, 
   3.718382086230236*^9}},ExpressionUUID->"d5af08ab-8d00-4963-af3f-\
773d637f30e2"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"y = \"\>", "\[InvisibleSpace]", 
   RowBox[{
    FractionBox[
     SubscriptBox["e", "7"], "2"], "-", 
    FractionBox[
     RowBox[{"5", " ", 
      SubscriptBox["e", "8"]}], "2"], "+", 
    FractionBox[
     SubscriptBox["e", "9"], "2"]}]}],
  SequenceForm[
  "y = ", Rational[1, 2] Subscript[$CellContext`e, 7] + 
   Rational[-5, 2] Subscript[$CellContext`e, 8] + 
   Rational[1, 2] Subscript[$CellContext`e, 9]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.718379901375916*^9, 3.718380163844893*^9, 3.718381174882799*^9, 
   3.718381272046136*^9, {3.718381775332274*^9, 3.718381798708577*^9}, 
   3.7183818327420177`*^9, {3.7183820813600073`*^9, 
   3.718382086587632*^9}},ExpressionUUID->"0afdaf0e-f419-4566-bee9-\
7f252ffb8ae2"]
}, {2}]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435", "Section",
 CellChangeTimes->{{3.716646160199793*^9, 3.716646165043662*^9}, {
   3.716734600686346*^9, 3.716734608587257*^9}, {3.7178650570935907`*^9, 
   3.7178650582335978`*^9}, 
   3.717922522247588*^9},ExpressionUUID->"8c178dec-d468-439d-aef4-\
0f7d38b23ab3"],

Cell[TextData[{
 "\:041a\:043e\:0441\:0438\:043d\:0443\:0441 \:0443\:0433\:043b\:0430 \:043c\
\:0435\:0436\:0434\:0443 x \:0438 y \
\:0432\:044b\:0447\:0438\:0441\:043b\:044f\:0435\:0442\:0441\:044f \:043a\
\:0430\:043a ",
 Cell[BoxData[
  FormBox[
   FractionBox[
    RowBox[{"(", 
     RowBox[{"x", ",", "y"}], ")"}], 
    RowBox[{"||", "x", "||", 
     RowBox[{"*", 
      RowBox[{"||", "y", "||"}]}]}]], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "b39972ff-fe83-40fa-a04e-1cfa2f7f02fa"],
 ". \:0410\:043d\:0430\:043b\:043e\:0433\:0438\:0447\:043d\:043e \:0437\:0430\
\:0434\:0430\:043d\:0438\:044e 31, \:0438\:0441\:043f\:043e\:043b\:044c\:0437\
\:0443\:0435\:043c \:043b\:0438\:043d\:0435\:0439\:043d\:043e\:0441\:0442\
\:044c \:0441\:043a\:0430\:043b\:044f\:0440\:043d\:043e\:0433\:043e \:043f\
\:0440\:043e\:0438\:0437\:0432\:0435\:0434\:0435\:043d\:0438\:044f \:0438 \
\:043e\:0440\:0442\:043e\:043d\:043e\:0440\:043c\:0438\:0440\:043e\:0432\:0430\
\:043d\:043d\:043e\:0441\:0442\:044c \:0441\:0438\:0441\:0442\:0435\:043c\
\:044b:"
}], "Text",
 CellChangeTimes->{{3.7183819559312563`*^9, 3.718382028449736*^9}, {
  3.718382122239059*^9, 
  3.718382156402327*^9}},ExpressionUUID->"b910a1f5-00c6-47d6-997d-\
93c385aa4fee"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"xy", "=", 
   RowBox[{
    FractionBox["1", "4"], "-", 
    RowBox[{
     FractionBox["2", "5"], " ", 
     FractionBox["5", "2"]}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<(x,y) = \>\"", ",", " ", "xy"}], "]"}]}], "Input",
 CellChangeTimes->{{3.718382054294117*^9, 3.718382059018426*^9}, {
  3.718382114221847*^9, 3.718382118971669*^9}, {3.718382180019061*^9, 
  3.718382259511158*^9}, {3.718382458541325*^9, 
  3.718382467228056*^9}},ExpressionUUID->"89e62616-e5a2-447f-a68b-\
054eab707dcb"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"(x,y) = \"\>", "\[InvisibleSpace]", 
   RowBox[{"-", 
    FractionBox["3", "4"]}]}],
  SequenceForm["(x,y) = ", 
   Rational[-3, 4]],
  Editable->False]], "Print",
 CellChangeTimes->{3.718382259937744*^9, 
  3.718382467946899*^9},ExpressionUUID->"e0ccfeb2-0cee-4ad9-adb6-\
207e74956562"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"xx", "=", 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     FractionBox["1", "9"], "+", 
     FractionBox["1", "4"], "+", 
     FractionBox["4", "25"]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<(x,x) = \>\"", ",", " ", "xx"}], "]"}]}], "Input",
 CellChangeTimes->{{3.7183822637634563`*^9, 3.718382304041745*^9}, {
  3.7183824801379757`*^9, 
  3.718382483416737*^9}},ExpressionUUID->"4d8b4194-4fbd-45eb-8e46-\
4acc1be172b5"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"(x,x) = \"\>", "\[InvisibleSpace]", 
   FractionBox[
    SqrtBox["469"], "30"]}],
  SequenceForm["(x,x) = ", Rational[1, 30] 469^Rational[1, 2]],
  Editable->False]], "Print",
 CellChangeTimes->{3.7183823053453903`*^9, 
  3.71838248387504*^9},ExpressionUUID->"ee408f15-dace-49ee-aaad-a91aa677ebac"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"yy", "=", 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     FractionBox["1", "4"], "+", 
     FractionBox["25", "4"], "+", 
     FractionBox["1", "4"]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<(y,y) = \>\"", ",", " ", "yy"}], "]"}]}], "Input",
 CellChangeTimes->{{3.7183822637634563`*^9, 3.7183823451081867`*^9}, {
  3.7183824870769*^9, 
  3.718382489810651*^9}},ExpressionUUID->"e6d7c75f-ccd2-4ad8-8542-\
73f4600ca6ba"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"(y,y) = \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{"3", " ", 
     SqrtBox["3"]}], "2"]}],
  SequenceForm["(y,y) = ", Rational[3, 2] 3^Rational[1, 2]],
  Editable->False]], "Print",
 CellChangeTimes->{3.7183823053453903`*^9, 3.7183823455080233`*^9, 
  3.718382490349921*^9},ExpressionUUID->"ad533d0f-8c68-4ce4-9ac9-\
cb06961a7217"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"cos", " ", "=", " ", 
   RowBox[{"xy", "/", 
    RowBox[{"(", 
     RowBox[{"xx", "*", "yy"}], ")"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<cos(\[Gamma]) = \>\"", ",", "  ", "cos"}], "]"}]}], "Input",
 CellChangeTimes->{{3.718382361846444*^9, 3.718382371586954*^9}, {
   3.718382406587584*^9, 3.718382435145287*^9}, 3.718382536685419*^9, 
   3.7183825798246098`*^9, {3.718382620000502*^9, 
   3.718382637950879*^9}},ExpressionUUID->"c708fba1-1916-4e41-89e8-\
c7399dd263a8"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"cos(\[Gamma]) = \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", "5"}], " ", 
    SqrtBox[
     FractionBox["3", "469"]]}]}],
  SequenceForm["cos(\[Gamma]) = ", (-5) Rational[3, 469]^Rational[1, 2]],
  Editable->False]], "Print",
 CellChangeTimes->{{3.718382413875485*^9, 3.718382435825067*^9}, 
   3.718382493763269*^9, 3.718382561588414*^9, 
   3.718382638522821*^9},ExpressionUUID->"177362d1-28cc-4731-9238-\
c563b9344f63"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<\:0422\:043e\:0433\:0434\:0430 \[Gamma] = arccos(\>\"", ",", "cos", 
   ",", "\"\<) = \>\"", ",", " ", 
   RowBox[{"N", "[", 
    RowBox[{"ArcCos", "[", "cos", "]"}], "]"}], ",", " ", 
   "\"\< \:0440\:0430\:0434\:0438\:0430\:043d.\>\""}], "]"}]], "Input",
 CellChangeTimes->{{3.718382564776155*^9, 
  3.718382697793291*^9}},ExpressionUUID->"bc941db6-430f-43ae-8829-\
6b32a1b5af0d"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\:0422\:043e\:0433\:0434\:0430 \[Gamma] = arccos(\"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", "5"}], " ", 
    SqrtBox[
     FractionBox["3", "469"]]}], "\[InvisibleSpace]", "\<\") = \"\>", 
   "\[InvisibleSpace]", "1.9821968394730416`", 
   "\[InvisibleSpace]", "\<\" \:0440\:0430\:0434\:0438\:0430\:043d.\"\>"}],
  SequenceForm[
  "\:0422\:043e\:0433\:0434\:0430 \[Gamma] = arccos(", (-5) 
   Rational[3, 469]^Rational[1, 2], ") = ", 1.9821968394730416`, 
   " \:0440\:0430\:0434\:0438\:0430\:043d."],
  Editable->False]], "Print",
 CellChangeTimes->{{3.71838266225912*^9, 
  3.718382698314991*^9}},ExpressionUUID->"1d525be7-332b-4085-9593-\
6f6bdb193a48"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1918, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/print.pdf"},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 854, 13, 92, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1459, 39, 187, 3, 68, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[1649, 44, 1030, 22, 60, "Text",ExpressionUUID->"6d37a514-bbac-4875-b7ec-8b520ed57479"],
Cell[CellGroupData[{
Cell[2704, 70, 694, 18, 55, "Input",ExpressionUUID->"1a45e8f0-ead0-4c4b-a1a9-098db961c574"],
Cell[3401, 90, 704, 20, 42, "Print",ExpressionUUID->"e0110ac2-0d05-4548-92fe-48a0b504e6e2"]
}, {2}]],
Cell[CellGroupData[{
Cell[4139, 115, 710, 18, 72, "Input",ExpressionUUID->"d5af08ab-8d00-4963-af3f-773d637f30e2"],
Cell[4852, 135, 793, 21, 42, "Print",ExpressionUUID->"0afdaf0e-f419-4566-bee9-7f252ffb8ae2"]
}, {2}]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5691, 162, 314, 5, 68, "Section",ExpressionUUID->"8c178dec-d468-439d-aef4-0f7d38b23ab3"],
Cell[6008, 169, 1256, 27, 69, "Text",ExpressionUUID->"b910a1f5-00c6-47d6-997d-93c385aa4fee"],
Cell[CellGroupData[{
Cell[7289, 200, 563, 14, 72, "Input",ExpressionUUID->"89e62616-e5a2-447f-a68b-054eab707dcb"],
Cell[7855, 216, 336, 10, 42, "Print",ExpressionUUID->"e0ccfeb2-0cee-4ad9-adb6-207e74956562"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8228, 231, 492, 13, 72, "Input",ExpressionUUID->"4d8b4194-4fbd-45eb-8e46-4acc1be172b5"],
Cell[8723, 246, 347, 8, 47, "Print",ExpressionUUID->"ee408f15-dace-49ee-aaad-a91aa677ebac"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9107, 259, 490, 13, 72, "Input",ExpressionUUID->"e6d7c75f-ccd2-4ad8-8542-73f4600ca6ba"],
Cell[9600, 274, 394, 10, 47, "Print",ExpressionUUID->"ad533d0f-8c68-4ce4-9ac9-cb06961a7217"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10031, 289, 545, 12, 55, "Input",ExpressionUUID->"c708fba1-1916-4e41-89e8-c7399dd263a8"],
Cell[10579, 303, 484, 12, 50, "Print",ExpressionUUID->"177362d1-28cc-4731-9238-c563b9344f63"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11100, 320, 438, 10, 31, "Input",ExpressionUUID->"bc941db6-430f-43ae-8829-6b32a1b5af0d"],
Cell[11541, 332, 724, 17, 50, "Print",ExpressionUUID->"1d525be7-332b-4085-9593-6f6bdb193a48"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

