(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     17527,        427]
NotebookOptionsPosition[     15568,        385]
NotebookOutlinePosition[     16107,        405]
CellTagsIndexPosition[     16064,        402]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 6, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, {3.72478326559545*^9, 
   3.724783265842194*^9}},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:041f\:0440\:0438\:0432\:0435\:0441\:0442\:0438 \:043f\:0440\:0438\:043c\
\:0435\:0440 \:0444\:0443\:043d\:043a\:0446\:0438\:0438 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"x", " ", "=", " ", 
    RowBox[{"x", 
     RowBox[{"(", "t", ")"}]}]}], TraditionalForm]],ExpressionUUID->
  "397b8f01-8c3e-45a5-bb03-e544e03a581d"],
 ", \:0443\:0434\:043e\:0432\:043b\:0435\:0442\:0432\:043e\:0440\:044f\:044e\
\:0449\:0435\:0439 \:043e\:0434\:043d\:043e\:0432\:0440\:0435\:043c\:0435\
\:043d\:043d\:043e \:0434\:0432\:0443\:043c \:0443\:0441\:043b\:043e\:0432\
\:0438\:044f\:043c (\:043f\:043e\:044f\:0441\:043d\:0438\:0442\:044c \:043f\
\:0440\:0438\:043c\:0435\:0440 \:0432\:044b\:0447\:0438\:0441\:043b\:0435\
\:043d\:0438\:044f\:043c\:0438)."
}], "Text",
 CellChangeTimes->{{3.7166436671156588`*^9, 3.716643668825863*^9}, {
   3.716643791825436*^9, 3.716643899003908*^9}, {3.7166447665590467`*^9, 
   3.7166447714880867`*^9}, {3.716646892984664*^9, 3.716646933045698*^9}, {
   3.7166469723179407`*^9, 3.7166469915130367`*^9}, 3.716647022269086*^9, {
   3.716647083530993*^9, 3.716647099021913*^9}, {3.716732667828871*^9, 
   3.7167327535942087`*^9}, 3.7167329221263323`*^9, {3.7167336068512783`*^9, 
   3.716733621660479*^9}, {3.716734261385623*^9, 3.716734391780354*^9}, {
   3.7247784329481487`*^9, 
   3.724778472324069*^9}},ExpressionUUID->"7d97eb88-6c84-495c-8987-\
76ff4b547bec"],

Cell[TextData[Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{"x", "\[Element]", 
    RowBox[{
     SuperscriptBox["L", "1"], "(", 
     RowBox[{"1", ";", 
      RowBox[{"+", "\[Infinity]"}]}], ")"}]}], ",", " ", 
   RowBox[{"x", "\[NotElement]", 
    RowBox[{
     SuperscriptBox["L", "1"], "(", 
     RowBox[{"0", ";", 
      RowBox[{"+", "\[Infinity]"}]}], ")"}]}]}], 
  TraditionalForm]],ExpressionUUID->"b2244143-9feb-4b7a-9ddd-0103e892d9a3"]], \
"Text",
 CellChangeTimes->{{3.724778475128292*^9, 3.724778509072891*^9}, {
  3.7247786268206577`*^9, 
  3.7247786286877403`*^9}},ExpressionUUID->"0dff58ce-f54c-4426-b957-\
682d7475ef90"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435", "Section",
 CellChangeTimes->{{3.716646160199793*^9, 3.716646165043662*^9}, {
  3.716734600686346*^9, 3.716734608587257*^9}, {3.724778522397407*^9, 
  3.724778523736287*^9}},ExpressionUUID->"8c178dec-d468-439d-aef4-\
0f7d38b23ab3"],

Cell["\:0417\:0430\:0434\:0430\:043d\:043d\:044b\:043c \:0443\:0441\:043b\
\:043e\:0432\:0438\:044f\:043c \:043f\:043e\:0434\:0445\:043e\:0434\:0438\
\:0442 \:0441\:043b\:0435\:0434\:0443\:044e\:0449\:0430\:044f \:0444\:0443\
\:043d\:043a\:0446\:0438\:044f:", "Text",
 CellChangeTimes->{{3.724779086341712*^9, 
  3.724779100716669*^9}},ExpressionUUID->"8b5c3299-f51f-46ef-862b-\
0a03a7cf7da2"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"x", "[", "t_", "]"}], ":=", 
   FractionBox["1", 
    SuperscriptBox["t", "2"]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<\!\(\*Cell[\"x(t) = \
\",ExpressionUUID->\"607020db-99f8-48b2-832d-c70cc24f69e2\"]\)\>\"", ",", 
   RowBox[{"TraditionalForm", "[", 
    RowBox[{"x", "[", "t", "]"}], "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.724778708867669*^9, 
  3.724778767460696*^9}},ExpressionUUID->"03fc3d82-c1bb-4c27-beca-\
582beccc6039"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\\!\\(\\*Cell[\\\"x(t) = \\\"]\\)\"\>", "\[InvisibleSpace]", 
   TagBox[
    FormBox[
     FractionBox["1", 
      SuperscriptBox["t", "2"]],
     TraditionalForm],
    TraditionalForm,
    Editable->True]}],
  SequenceForm["\!\(\*Cell[\"x(t) = \"]\)", 
   TraditionalForm[$CellContext`t^(-2)]],
  Editable->False]], "Print",
 CellChangeTimes->{{3.724778745643146*^9, 3.724778767797997*^9}, 
   3.72477912650839*^9},ExpressionUUID->"9f9c7b5c-2fb0-4997-aeb5-\
d133f02bd9d5"]
}, {2}]],

Cell[TextData[Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    SubsuperscriptBox["\[Integral]", "1", "\[Infinity]"], 
    RowBox[{
     FractionBox["1", 
      SuperscriptBox["t", "2"]], 
     RowBox[{"\[DifferentialD]", "t"}]}]}], " ", "=", 
   RowBox[{
    TemplateBox[{RowBox[{"(", 
        RowBox[{"1", "-", 
          FractionBox["1", "A"]}], ")"}],"A","\[Infinity]"},
     "Limit2Arg",
     DisplayFunction->(RowBox[{
        TagBox[
         UnderscriptBox[
          StyleBox["\"lim\"", ShowStringCharacters -> False], 
          RowBox[{#2, "\[Rule]", #3}], LimitsPositioning -> True], Identity, 
         SyntaxForm -> "\[Limit]", Tooltip -> "Limit"], #}]& ),
     InterpretationFunction->(RowBox[{"Limit", "[", 
        RowBox[{#, ",", 
          RowBox[{#2, "->", #3}]}], "]"}]& )], "=", " ", 
    RowBox[{"1", " ", "\[Rule]", " ", 
     FormBox[
      RowBox[{"x", "\[Element]", 
       RowBox[{
        SuperscriptBox["L", "1"], 
        RowBox[{"(", 
         RowBox[{"1", ";", 
          RowBox[{"+", "\[Infinity]"}]}], ")"}]}]}],
      TraditionalForm]}]}]}], 
  TraditionalForm]],ExpressionUUID->"693acd17-a8c7-4569-971b-bc026a59d83a"]], \
"Text",
 CellChangeTimes->{{3.72477907065136*^9, 3.724779073556528*^9}, {
  3.7247792710450153`*^9, 3.724779308040366*^9}, {3.724779355185277*^9, 
  3.724779407729282*^9}, {3.7247794566892853`*^9, 
  3.724779459917797*^9}},ExpressionUUID->"49f87991-e461-457f-bc3e-\
bc38277557d8"],

Cell[TextData[Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    SubsuperscriptBox["\[Integral]", "0", "\[Infinity]"], 
    RowBox[{
     FractionBox["1", 
      SuperscriptBox["t", "2"]], 
     RowBox[{"\[DifferentialD]", "t"}]}]}], " ", "=", " ", 
   RowBox[{"\[Infinity]", " ", "\[Rule]", " ", 
    FormBox[
     RowBox[{"x", "\[NotElement]", 
      RowBox[{
       SuperscriptBox["L", "1"], "(", 
       RowBox[{"0", ";", 
        RowBox[{"+", "\[Infinity]"}]}], ")"}]}],
     TraditionalForm]}]}], 
  TraditionalForm]],ExpressionUUID->"730aa89f-856f-4e74-a43a-0c1f59be2a4f"]], \
"Text",
 CellChangeTimes->{{3.724779028568767*^9, 
  3.72477903314056*^9}},ExpressionUUID->"4a83a623-db4b-45c9-98ed-\
993a5cfae201"],

Cell["\<\
\:041c\:043e\:0434\:0443\:043b\:0438 \:043e\:043f\:0443\:0449\:0435\:043d\
\:044b, \:0442\:0430\:043a \:043a\:0430\:043a \:043d\:0430 \:0437\:0430\:0434\
\:0430\:043d\:043d\:044b\:0445 \:043f\:0440\:043e\:043c\:0435\:0436\:0443\
\:0442\:043a\:0430\:0445 \:0444\:0443\:043d\:043a\:0446\:0438\:044f \:043d\
\:0435 \:043f\:0440\:0438\:043d\:0438\:043c\:0430\:0435\:0442 \:043e\:0442\
\:0440\:0438\:0446\:0430\:0442\:0435\:043b\:044c\:043d\:044b\:0445 \:0437\
\:043d\:0430\:0447\:0435\:043d\:0438\:0439.\
\>", "Text",
 CellChangeTimes->{{3.724779185403701*^9, 
  3.72477921985712*^9}},ExpressionUUID->"0204381e-876b-42a9-8498-\
109cfe63447b"],

Cell["\:0414\:043b\:044f \:043d\:0430\:0433\:043b\:044f\:0434\:043d\:043e\
\:0441\:0442\:0438, \:043c\:043e\:0436\:043d\:043e \:043f\:043e\:0441\:0442\
\:0440\:043e\:0438\:0442\:044c \:0433\:0440\:0430\:0444\:0438\:043a:", "Text",\

 CellChangeTimes->{{3.72477913836594*^9, 
  3.7247791461289873`*^9}},ExpressionUUID->"fa1f3108-1d09-41c4-8a74-\
a50e05a32d59"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"$FrontEndSession", ",", 
    RowBox[{"ShowCellLabel", "\[Rule]", "False"}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Plot", "[", 
  RowBox[{
   FractionBox["1", 
    SuperscriptBox["t", "2"]], ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "8"}], "}"}], ",", 
   RowBox[{"ImageSize", "\[Rule]", "Large"}], ",", 
   RowBox[{"AxesLabel", "\[Rule]", "Automatic"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.724778974530436*^9, 3.724778975070652*^9}, {
  3.7247832303161097`*^9, 3.7247832413887*^9}, {3.724819450339356*^9, 
  3.724819457510869*^9}},
 NumberMarks->False,ExpressionUUID->"5cc22199-71e0-43c4-bd2a-0adcc1d4613a"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwVlHc81fvjx49jhuxsnRS3TUPcNF5vJ0pGIioZlQaRUvmR0pVQEs4nqRsl
I6SFRCozySguWUeyR6Ej43Bsvv3+eD6e/zz/fqo7nbU6QafRaGl/+H//fdiv
tO0pB0uTrGRVhrmIx52UO184kK+4NeBdxMV03lXmIdUBZMZfYDvbcJH23s7r
Z94ARJSf90YFjUAhU7qVj28Qih7VD0+JDcM+dNq6W34QhSG5Qhe4Q0g43vOl
ZM0gwjcZN6R+H8KaRe/e3To4CPsYd8+cF0Ngeh25uyh9EAv1Jt8pWQ5B9+GB
5EjtIdTP9bgMGw7CNriiI0h/CAHKW8vrlAfh62mg5m00BHcWRV84/BsfzFZH
2toNYSS+6v8uPvoNs9k5/8U3hsCQPL952ewAjjomO6S0DqGZZrhKqpyDQBOV
qKi+IWgYLn/T/JiDZF2qLmR0CKzP8uuH/+GAI3HJ1F10GLu552QGN3HgVWC+
eYPuMETCLNmrnvxC6JIxudywYayprNUWv9ePN507Kqq2jmDROJW1Jr4XDcb+
w2G7RtC5tHMdM7AXvNR8eTOrEYhqJVwKce6F3uXNR8ucR9DgXBL7SqsXb2W1
eYURI/hpmn3DvOAn3hkqL8noHUFtMS/7UN8P5CYPXYiM5CLqU/DzeYceNItr
RVnFciFwc+JV3M4ezJx3y5d6xkVQBjXtrN2DbfghElbARerJHDEXvh7ks7/H
BPVz4dXttlnzSTcKREpLvcgo1Hc77beY7EKRa4yKLWcUy1SbC5zfdOJ1n5ub
I28UOhsNpIITO5Hoop9zjDaGm6H7hj5EdCLoJPvQGbkx7PE9FON/thPGTtLR
AVvHEMnq9Vy9qhNVtkGKqaFjEGt8UEtL7ECzsfsi/rU8qDDDSVBKOypLt5wQ
0ePBqertIf+oduTvFM1aaMDD7Uc9b2+HtCPW8Im1og0PS/wm0n6fbocT6biz
9goPf42Gx9/d0I5ePRuZg5U8nFlglDpyrQ1jf22TfOk+jhUylR6u31qwvZNZ
GnJxHAMx/b6VeS24HmPs5xIwDt7sybL9CS2Ql7MeXBY1jk0JcgLf3Vqgy+dW
FV08Dpv8G34JtBZc/H6fFawyAe+JVwu565oxwxqVPF4+gYjTL250P2uCoelU
qUHdBMZDjAwj7jQhVIh2ldE2gX0PNtxy9G2C2hWxoabRCVAktMtyTxO2uy6t
tlwyCd+N/cKPuN/gZ7iXgvckagK+Nsbu+Ab65HMpFc0pVDtkfZkZZ0OlksmF
9hS6b2zXPNPDhk78t/rjm6dQvvYO/1gNGyd2Cz9INZ+C9cHZcmYqG2VRTppM
rynsuGzyq+kEGyx9Jf1TpVOgH36d1dHYAFXf68eyT01DwV9fyruyHpv2qu1s
vjAN9UPh6x3y6rFHI3MF3z/TmGlgLrV5WQ+/io4Bk4hpnOuXWXAprB6dqtsu
tr6fRnpJitLdPfV4mscNFRSfwd+esiZTtXXQnT/yZl/qDGJzIdXUXwsz88Jr
Rm9nINKQf1y6pRbHHjAs9IpmEJ5uzX+gqhaUXutP5YYZCLFo1eKZteg7a6fU
OTsD/T32N5b8U4uH7Ta+HuazyBCQbmLL1YKvyIQZxpmF/03Nb8JmNVCUeibh
x5vFmb0zyd2ogZbjgu8etDnYcaIDKzfW4NBU6QVruTlUdp/T/qhSg8wNRkkq
W+fg0yB5ayvnK04+hsizW3PQDJLvt2V9RUWgzn8lq+bh1SBQ0thSDX3JL/Tb
m+bxEc4b19dUIyXqqJ4dmcfh38Kv7pZUIyA1PG7QZh4HdWKjr6X96Rt7zyv6
z+PBX/ft+/3/9KtjFFzZ8+COL0joWlENZt+1dCEmjTAXCt/XnPsPDOrX5NPf
NKJkqVOUF1GB/KN+TzO4NPJIu55e5l8Bx42ytjkTNLK4L/Zgh0cFHjbov62g
8xEbSdsZYlEBRbUQr0F5PmKgnaCbIV4BmWcruDrgI+rK/A7mIV8gWHyCU8Di
I2rVo2kjrM/4Nd7WWr+OTvgvpr5M+lCGN86sIhNdOgluCdpYk1GGq+ztyQVb
6MSKzhESTyyDfHaM+9OddGK65cf+J9fL/vzZbsbXnk6mtYy6L5mVIXq0QUkj
mE5CpdlGwU2lMB6utD7fRifJ08Y69fMlSOzP+SwRzk8CX3LZzNOfcOZfi2Wu
d/hJ3SR7IuTIJ+gZdl3+dJ+f1Gf6ZjZbf8LnGNG1vo/5iZORZs3DbZ8wvNeW
1feWn5zIaQ0Ml/yE7dm8fcVd/MR6ZKpbK6sY3wLWt/hsFiCexxN1GcLFkFB7
MtjdLUAqx27Sij8WgewPaOnqEyDu+ftF/3tfhPOsw186fwuQD4dKrrS+KkID
n+KT9gkB0hGgflMmrgiPfgQ7NosLEjf5E8ElvkXQSnetrNURJB47jo4d1CuC
+Q6t50WBgsR0efe++fQPCD2VdTJOQ4jIbogJ1cwqRD95GSy2Soi8KxQxnn5R
iF2KSc+8tIVIjZ/xXENiIeilkb/N9IWIuTRvR1xkIXw0PL0mLYRIatnZCB/P
Qpxs23Dd6rIQKQ+vM2JuKoSBTXqSQI0QefW9xE3MqwA88rzbxU+Y1L3795L7
izx4bijOlP8oQvrvOhj5vH2PttTiW1/VRYmao1qHwFw2pl3rHqy8JEYsEhXc
T7zOwmoD4bmTOeLEsfWdR17ja+xKUHd+pSBBbMq1tmcrZMBb7fDSDntJcmZV
r/ySlHSs4diIsc2lyMqcmlzTr6mQlRwSKZ6SIvZpC85Ju76AQMGDtGUPpcmx
rrpzsa7P4KaT822BqQzxe5J8nHMpBc93O8Xl82RIeKLlgGdpMsZ+KUv9uCdL
fPl86MMHk5C1bxd/pKEcuRP2hegMP4afcVBs5g85cljYhAooTIB6i0nj46BF
pKTfbmVYWjz09QIjlTbIE78yU5GuuDjYL66a5KuVJ+bbE5UsL8Ri7HNYp4e/
ArHtqasxexKD+wKOnlYaimQ/cSEeqg8h25qVQC9WJBbXkjb6vomG4BV/683n
lMigROPS+xZRoD11TOuUUSa1zT1KwUr3cT46LUymSJlszXbzpnXeQ97qYBHp
MypkbjOYYSV3IW1yfs01CVXCe29JdldHonltda1Priox3FnZYMC+g/RH0Oh2
VCPi28ZurJ+LgEOarrTanBqJEu0uj9ONwNWgeP/mZ4uJwrkRuQy323jZq/ZA
woxB1m5c3iX+nkJwTNCR1j0MYnzbe0Asm8JxqwHNVEsGqZBml4hlUlDNzU03
P8AgL22jexamUghh2ZWEOjFItIvSF5UECi66UcOiPgxSKRQ/5hBCYWmgnLFw
MoPYHymq8LGjMPe370J2CoNobPNeHXGQwreBrprk5wwy7LZu73MbCtSB1w47
XzGIc8G9/jYLCvOrrC4E5TLIdtFbdlaGFJq/sh7x1zKIG2/TsN1aCm+vjx+r
rWeQFbRrrKurKERuObzycSODlO33HUxaTsE0SSuT2cogUXdduSPqFN5frCz3
72OQ+Oj3/PfkKdxbu4llyWGQgouCKYWyFM51PrRWH2SQWcE2MY4UhRVmp9sK
Rxlk8b110kZiFAT46hKpcQbZprA047wIhfasLa5Hphikasdd6XhBCrmuj7XX
zTLIR41d2tV0Cv8yxMbm5xnkbPuoKI1G4X/hXkjD
       "]]},
     Annotation[#, "Charting`Private`Tag$2144#1"]& ]}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     TagBox["t", HoldForm], TraditionalForm], None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  ImageSize->Large,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 8}, {0., 1.2463637118147024`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.7247789649254103`*^9, 3.724778975512547*^9}, 
   3.724779148413072*^9, 3.724783251649805*^9, 
   3.724819458582163*^9},ExpressionUUID->"350e8afe-5462-43a0-a34b-\
d7a4d99c4f22"]
}, {2}]],

Cell[TextData[{
 "\:0412 \:0442\:043e\:0447\:043a\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"x", "=", "0"}], TraditionalForm]],ExpressionUUID->
  "add7ca8a-cec5-4bb4-a27c-d21a1ea6edc8"],
 " \:0444\:0443\:043d\:043a\:0446\:0438\:044f \:0442\:0435\:0440\:043f\:0438\
\:0442 \:0440\:0430\:0437\:0440\:044b\:0432, \:0441\:043b\:0435\:0434\:043e\
\:0432\:0430\:0442\:0435\:043b\:044c\:043d\:043e \:0438\:043d\:0442\:0435\
\:0433\:0440\:0430\:043b ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubsuperscriptBox["\[Integral]", "0", "\[Infinity]"], 
    RowBox[{
     FractionBox["1", 
      SuperscriptBox["t", "2"]], 
     RowBox[{"\[DifferentialD]", "t"}]}]}], TraditionalForm]],ExpressionUUID->
  "3a505ff2-0a6f-4915-994a-e74463ae82c7"],
 " \:043d\:0435 \:0441\:0445\:043e\:0434\:0438\:0442\:0441\:044f.\n"
}], "Text",
 CellChangeTimes->{{3.72477915276479*^9, 3.724779173297735*^9}, {
  3.724779229588993*^9, 
  3.724779255694141*^9}},ExpressionUUID->"fe18ec5b-d4d1-474a-9725-\
f9114486daef"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1918, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/print.pdf"},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 634, 10, 92, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1239, 36, 187, 3, 68, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[1429, 41, 1397, 25, 35, "Text",ExpressionUUID->"7d97eb88-6c84-495c-8987-76ff4b547bec"],
Cell[2829, 68, 636, 18, 35, "Text",ExpressionUUID->"0dff58ce-f54c-4426-b957-682d7475ef90"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3502, 91, 282, 4, 68, "Section",ExpressionUUID->"8c178dec-d468-439d-aef4-0f7d38b23ab3"],
Cell[3787, 97, 393, 6, 35, "Text",ExpressionUUID->"8b5c3299-f51f-46ef-862b-0a03a7cf7da2"],
Cell[CellGroupData[{
Cell[4205, 107, 523, 14, 76, "Input",ExpressionUUID->"03fc3d82-c1bb-4c27-beca-582beccc6039"],
Cell[4731, 123, 522, 15, 44, "Print",ExpressionUUID->"9f9c7b5c-2fb0-4997-aeb5-d133f02bd9d5"]
}, {2}]],
Cell[5265, 141, 1440, 38, 44, "Text",ExpressionUUID->"49f87991-e461-457f-bc3e-bc38277557d8"],
Cell[6708, 181, 716, 21, 42, "Text",ExpressionUUID->"4a83a623-db4b-45c9-98ed-993a5cfae201"],
Cell[7427, 204, 648, 11, 35, "Text",ExpressionUUID->"0204381e-876b-42a9-8498-109cfe63447b"],
Cell[8078, 217, 359, 6, 35, "Text",ExpressionUUID->"fa1f3108-1d09-41c4-8a74-a50e05a32d59"],
Cell[CellGroupData[{
Cell[8462, 227, 708, 17, 73, "Input",ExpressionUUID->"5cc22199-71e0-43c4-bd2a-0adcc1d4613a"],
Cell[9173, 246, 5363, 108, 361, "Output",ExpressionUUID->"350e8afe-5462-43a0-a34b-d7a4d99c4f22"]
}, {2}]],
Cell[14548, 357, 992, 24, 65, "Text",ExpressionUUID->"fe18ec5b-d4d1-474a-9725-f9114486daef"]
}, Open  ]]
}, Open  ]]
}
]
*)

