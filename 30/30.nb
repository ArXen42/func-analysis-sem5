(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     14901,        402]
NotebookOptionsPosition[     12869,        356]
NotebookOutlinePosition[     13408,        376]
CellTagsIndexPosition[     13365,        373]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[TextData[StyleBox["\:041f\:0435\:043b\:0430\:0433\:0435\:0439\:043a\:0438\
\:043d \:0413\:0435\:043e\:0440\:0433\:0438\:0439, 33506/1.\n\:0417\:0430\
\:0434\:0430\:043d\:0438\:0435 30, \:0432\:0430\:0440\:0438\:0430\:043d\:0442 \
13.",
 FontSize->18]], "Title",
 CellChangeTimes->{{3.716640188244278*^9, 3.716640226122375*^9}, 
   3.716640545927516*^9, {3.716646541593095*^9, 3.7166465452709217`*^9}, {
   3.71664687772606*^9, 3.716646878783936*^9}, 3.7167325216742983`*^9, {
   3.716734134363058*^9, 3.716734134821053*^9}, 3.717786296159808*^9, {
   3.71786504637932*^9, 3.7178650467360163`*^9}, {3.717919799320438*^9, 
   3.717919799615849*^9}, {3.71792363608844*^9, 3.717923636650264*^9}, {
   3.718475708618*^9, 
   3.718475709757411*^9}},ExpressionUUID->"6d1a581f-e7ce-4e79-952f-\
ec08dbc226d7"],

Cell[CellGroupData[{

Cell["\:0423\:0441\:043b\:043e\:0432\:0438\:0435", "Section",
 CellChangeTimes->{{3.716642601428467*^9, 
  3.716642603163493*^9}},ExpressionUUID->"d2acd2e8-35fa-4520-8268-\
62e8660501ed"],

Cell[TextData[{
 "\:0417\:0430\:043f\:0438\:0441\:0430\:0442\:044c \:043d\:0435\:0440\:0430\
\:0432\:0435\:043d\:0441\:0442\:0432\:043e \:041a\:043e\:0448\:0438 - \:0411\
\:0443\:043d\:044f\:043a\:043e\:0432\:0441\:043a\:043e\:0433\:043e \:0432 \
\:043f\:0440\:043e\:0441\:0442\:0440\:0430\:043d\:0441\:0442\:0432\:0435 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["L", "2"], "(", 
    RowBox[{"a", ";", "b"}], ")"}], TraditionalForm]],ExpressionUUID->
  "d536cec4-9dc3-4a34-b98e-03e26475fa34"],
 " \:0438 \:0443\:0431\:0435\:0434\:0438\:0442\:044c\:0441\:044f, \
\:0447\:0442\:043e \:043e\:043d\:043e \:0432\:044b\:043f\:043e\:043b\:043d\
\:0435\:043d\:043e \:0434\:043b\:044f \:0434\:0430\:043d\:043d\:044b\:0445 \
\:0444\:0443\:043d\:043a\:0446\:0438\:0439 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"x", "=", 
    RowBox[{"x", "(", "t", ")"}]}], TraditionalForm]],ExpressionUUID->
  "470203c4-ebac-47c7-b0fa-8b15e43d9231"],
 " \:0438 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"y", "=", 
    RowBox[{"y", "(", "t", ")"}]}], TraditionalForm]],ExpressionUUID->
  "9387ea58-bf87-40f8-bc0f-f49fbf6c7da4"],
 ".\n(a;b) = ",
 Cell[BoxData[
  FormBox[
   RowBox[{"(", 
    RowBox[{"0", ";", 
     FractionBox["\[Pi]", "4"]}], ")"}], TraditionalForm]],ExpressionUUID->
  "4cc888f5-ac14-4789-9333-0c213d8a70bc"],
 "."
}], "Text",
 CellChangeTimes->{{3.7179198292522507`*^9, 3.7179199083676767`*^9}, {
  3.717923644582842*^9, 3.717923707295753*^9}, {3.717924414566681*^9, 
  3.717924424287899*^9}, {3.717925966959094*^9, 3.717925969265983*^9}, {
  3.7184757275622063`*^9, 3.718475775276905*^9}, {3.718475853860371*^9, 
  3.7184758611205063`*^9}, {3.718475907937685*^9, 
  3.718475908864943*^9}},ExpressionUUID->"82f75743-f8c4-4b40-a90d-\
70e69efa8fed"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"x", "[", "t_", "]"}], "=", 
   FractionBox["1", 
    RowBox[{"Cos", "[", "t", "]"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<x(t) = \>\"", ",", " ", 
   RowBox[{"TraditionalForm", "[", 
    RowBox[{"x", "[", "t", "]"}], "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.717919909701922*^9, 3.7179199937092953`*^9}, {
  3.71792011546497*^9, 3.717920154704398*^9}, {3.717922959330933*^9, 
  3.717922961782872*^9}, {3.71792374513107*^9, 3.71792376500767*^9}, {
  3.718475803029026*^9, 
  3.7184758117933807`*^9}},ExpressionUUID->"0465bf6d-d5b3-451e-9283-\
ceda9e6776c4"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"x(t) = \"\>", "\[InvisibleSpace]", 
   TagBox[
    FormBox[
     RowBox[{"sec", "(", "t", ")"}],
     TraditionalForm],
    TraditionalForm,
    Editable->True]}],
  SequenceForm["x(t) = ", 
   TraditionalForm[
    Sec[$CellContext`t]]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.7179229645597677`*^9, 3.7179237735669203`*^9, 3.717924380171534*^9, 
   3.717925089352344*^9, 3.717925514335866*^9, {3.7179259054151163`*^9, 
   3.717925921925241*^9}, {3.71847579000385*^9, 3.718475812324058*^9}, 
   3.718475915392058*^9, 3.718476570976183*^9, 3.718533850467325*^9, 
   3.7185339394385242`*^9},ExpressionUUID->"6fbf1267-b302-4550-a7a9-\
1ae3c2c19928"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{" ", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"y", "[", "t_", "]"}], "=", 
     RowBox[{"Sin", "[", "t", "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"Print", "[", 
    RowBox[{"\"\<y(t) = \>\"", ",", " ", 
     RowBox[{"TraditionalForm", "[", 
      RowBox[{"y", "[", "t", "]"}], "]"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.718475829309924*^9, 
  3.7184758381100283`*^9}},ExpressionUUID->"f688561a-85dd-4134-bb58-\
3cbcb7a7042c"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"y(t) = \"\>", "\[InvisibleSpace]", 
   TagBox[
    FormBox[
     RowBox[{"sin", "(", "t", ")"}],
     TraditionalForm],
    TraditionalForm,
    Editable->True]}],
  SequenceForm["y(t) = ", 
   TraditionalForm[
    Sin[$CellContext`t]]],
  Editable->False]], "Print",
 CellChangeTimes->{{3.7179201399705067`*^9, 3.71792015538391*^9}, {
   3.71792251960884*^9, 3.7179225260944853`*^9}, 3.717922965224895*^9, 
   3.717923774229886*^9, 3.7179243802949457`*^9, 3.717925089500268*^9, 
   3.71792551444313*^9, {3.717925905456893*^9, 3.717925922089231*^9}, 
   3.718475790162272*^9, 3.718475838685001*^9, 3.718475915443572*^9, 
   3.718476571080083*^9, 3.718533850579083*^9, 
   3.718533939655311*^9},ExpressionUUID->"504c10de-7ce5-459e-b134-\
5d4d56319a37"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0435\:0448\:0435\:043d\:0438\:0435", "Section",
 CellChangeTimes->{{3.716646160199793*^9, 3.716646165043662*^9}, {
   3.716734600686346*^9, 3.716734608587257*^9}, {3.7178650570935907`*^9, 
   3.7178650582335978`*^9}, 
   3.717922522247588*^9},ExpressionUUID->"8c178dec-d468-439d-aef4-\
0f7d38b23ab3"],

Cell["\:041d\:0435\:0440\:0430\:0432\:0435\:043d\:0441\:0442\:0432\:043e \
\:041a\:043e\:0448\:0438 - \:0411\:0443\:043d\:044f\:043a\:043e\:0432\:0441\
\:043a\:043e\:0433\:043e \:0432 \:043d\:0430\:0448\:0435\:043c \:0441\:043b\
\:0443\:0447\:0430\:0435 \:0438\:043c\:0435\:0435\:0442 \:0432\:0438\:0434", \
"Text",
 CellChangeTimes->{{3.7184759654175987`*^9, 3.718475996795178*^9}, {
   3.718476077772044*^9, 3.7184761454900913`*^9}, {3.718476432863872*^9, 
   3.718476444932028*^9}, 3.718476477203272*^9, {3.7184765217846413`*^9, 
   3.718476525427525*^9}},ExpressionUUID->"3c360039-72e7-47b6-a45f-\
93861380542b"],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    SubsuperscriptBox["\[Integral]", "a", "b"], 
    RowBox[{
     RowBox[{"x", "(", "t", ")"}], " ", 
     RowBox[{"y", "(", "t", ")"}], 
     RowBox[{"\[DifferentialD]", "t"}]}]}], "\[LessEqual]", 
   RowBox[{
    SqrtBox[
     RowBox[{
      SubsuperscriptBox["\[Integral]", "a", "b"], 
      RowBox[{
       RowBox[{"x", "(", "t", ")"}], 
       RowBox[{"\[DifferentialD]", "t"}]}]}]], " ", 
    RowBox[{
     SqrtBox[
      RowBox[{
       SubsuperscriptBox["\[Integral]", "a", "b"], 
       RowBox[{
        RowBox[{"y", "(", "t", ")"}], 
        RowBox[{"\[DifferentialD]", "t"}]}]}]], "."}]}]}], 
  TraditionalForm]], "Text",
 CellChangeTimes->{{3.7184761575523252`*^9, 3.71847619011875*^9}, {
   3.718476339250452*^9, 3.718476417738929*^9}, {3.7184764571657877`*^9, 
   3.718476460008689*^9}, {3.718476510343801*^9, 3.7184765365744343`*^9}, 
   3.71847657648184*^9},ExpressionUUID->"90595880-da00-429c-89bd-\
765e2feda9c7"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"a", "=", "0"}], ";", " ", 
  RowBox[{"b", "=", 
   RowBox[{"Pi", "/", "4"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"lhs", "=", 
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"x", "[", "t", "]"}], "*", 
      RowBox[{"y", "[", "t", "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", "a", ",", "b"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"rhs", "=", 
   RowBox[{
    RowBox[{"Sqrt", "[", 
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"x", "[", "t", "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"t", ",", "a", ",", "b"}], "}"}]}], "]"}], "]"}], "*", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"y", "[", "t", "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"t", ",", "a", ",", "b"}], "}"}]}], "]"}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"lhs", ",", " ", "\"\< \[LessEqual] \>\"", ",", " ", "rhs"}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.7185338436836653`*^9, 
  3.718533927648014*^9}},ExpressionUUID->"be8415f6-d1bb-40ce-b7c9-\
f29341beee42"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   FractionBox[
    RowBox[{"Log", "[", "2", "]"}], "2"], 
   "\[InvisibleSpace]", "\<\" \[LessEqual] \"\>", "\[InvisibleSpace]", 
   SqrtBox[
    RowBox[{"2", " ", 
     RowBox[{"(", 
      RowBox[{"1", "-", 
       FractionBox["1", 
        SqrtBox["2"]]}], ")"}], " ", 
     RowBox[{"ArcTanh", "[", 
      RowBox[{"Tan", "[", 
       FractionBox["\[Pi]", "8"], "]"}], "]"}]}]]}],
  SequenceForm[
  Rational[1, 2] Log[2], 
   " \[LessEqual] ", (2 (1 - 2^Rational[-1, 2]) ArcTanh[
      Tan[Rational[1, 8] Pi]])^Rational[1, 2]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.718533850588032*^9, {3.7185339240248337`*^9, 
   3.718533939880612*^9}},ExpressionUUID->"e950f94e-2166-4178-a1f9-\
c059fcf1861b"]
}, {2}]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"diff", "=", 
   RowBox[{"lhs", "-", "rhs"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<\:0420\:0430\:0437\:043d\:043e\:0441\:0442\:044c \:043b\:0435\:0432\
\:043e\:0439 \:0438 \:043f\:0440\:0430\:0432\:043e\:0439 \:0447\:0430\:0441\
\:0442\:0438 \:0431\:0443\:0434\:0435\:0442 \:0440\:0430\:0432\:043d\:0430 \>\
\"", ",", "diff", ",", " ", "\"\< = \>\"", ",", " ", 
   RowBox[{"N", "[", "diff", "]"}], ",", " ", "\"\<.\>\""}], "]"}]}], "Input",\

 CellChangeTimes->{{3.718476651961109*^9, 3.7184766605230293`*^9}, {
   3.7184766994776173`*^9, 3.718476719161866*^9}, 3.718533860967766*^9, {
   3.718533929651477*^9, 
   3.718533931530118*^9}},ExpressionUUID->"bcc88dc9-9c9c-4a40-bef0-\
68881058ab69"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"\:0420\:0430\:0437\:043d\:043e\:0441\:0442\:044c \:043b\:0435\
\:0432\:043e\:0439 \:0438 \:043f\:0440\:0430\:0432\:043e\:0439 \:0447\:0430\
\:0441\:0442\:0438 \:0431\:0443\:0434\:0435\:0442 \:0440\:0430\:0432\:043d\
\:0430 \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     SqrtBox[
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", 
         FractionBox["1", 
          SqrtBox["2"]]}], ")"}], " ", 
       RowBox[{"ArcTanh", "[", 
        RowBox[{"Tan", "[", 
         FractionBox["\[Pi]", "8"], "]"}], "]"}]}]]}], "+", 
    FractionBox[
     RowBox[{"Log", "[", "2", "]"}], "2"]}], 
   "\[InvisibleSpace]", "\<\" = \"\>", "\[InvisibleSpace]", 
   RowBox[{"-", "0.16150942151958808`"}], "\[InvisibleSpace]", "\<\".\"\>"}],
  SequenceForm[
  "\:0420\:0430\:0437\:043d\:043e\:0441\:0442\:044c \:043b\:0435\:0432\:043e\
\:0439 \:0438 \:043f\:0440\:0430\:0432\:043e\:0439 \:0447\:0430\:0441\:0442\
\:0438 \:0431\:0443\:0434\:0435\:0442 \:0440\:0430\:0432\:043d\:0430 ", -(
      2 (1 - 2^Rational[-1, 2]) ArcTanh[
        Tan[Rational[1, 8] Pi]])^Rational[1, 2] + Rational[1, 2] Log[2], 
   " = ", -0.16150942151958808`, "."],
  Editable->False]], "Print",
 CellChangeTimes->{{3.7184767010372562`*^9, 3.7184767198933277`*^9}, 
   3.718533852757352*^9, {3.7185339321668053`*^9, 
   3.718533939887465*^9}},ExpressionUUID->"4f991c7d-3d7a-4015-8e2e-\
9f8670698630"]
}, {2}]],

Cell[TextData[{
 "\:041f\:043e\:043b\:0443\:0447\:0435\:043d\:043d\:0430\:044f \:0440\:0430\
\:0437\:043d\:043e\:0441\:0442\:044c \:043e\:0442\:0440\:0438\:0446\:0430\
\:0442\:0435\:043b\:044c\:043d\:0430, \:0441\:043b\:0435\:0434\:043e\:0432\
\:0430\:0442\:0435\:043b\:044c\:043d\:043e ",
 StyleBox["\:043d\:0435\:0440\:0430\:0432\:0435\:043d\:0441\:0442\:0432\:043e \
\:0432\:044b\:043f\:043e\:043b\:043d\:044f\:0435\:0442\:0441\:044f",
  FontWeight->"Bold"],
 "."
}], "Text",
 CellChangeTimes->{{3.718476704265543*^9, 
  3.718476738834234*^9}},ExpressionUUID->"b77b38aa-65de-41d7-a089-\
623c50e849c2"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1918, 1058},
WindowMargins->{{1, Automatic}, {1, Automatic}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
PrintingOptions->{"PaperOrientation"->"Portrait",
"PaperSize"->{Automatic, Automatic},
"PostScriptOutputFile"->"/home/glados/print.pdf"},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 805, 13, 92, "Title",ExpressionUUID->"6d1a581f-e7ce-4e79-952f-ec08dbc226d7"],
Cell[CellGroupData[{
Cell[1410, 39, 187, 3, 68, "Section",ExpressionUUID->"d2acd2e8-35fa-4520-8268-62e8660501ed"],
Cell[1600, 44, 1754, 41, 67, "Text",ExpressionUUID->"82f75743-f8c4-4b40-a90d-70e69efa8fed"],
Cell[CellGroupData[{
Cell[3379, 89, 647, 15, 77, "Input",ExpressionUUID->"0465bf6d-d5b3-451e-9283-ceda9e6776c4"],
Cell[4029, 106, 709, 19, 25, "Print",ExpressionUUID->"6fbf1267-b302-4550-a7a9-1ae3c2c19928"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4775, 130, 476, 13, 55, "Input",ExpressionUUID->"f688561a-85dd-4134-bb58-3cbcb7a7042c"],
Cell[5254, 145, 800, 20, 25, "Print",ExpressionUUID->"504c10de-7ce5-459e-b134-5d4d56319a37"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6103, 171, 314, 5, 68, "Section",ExpressionUUID->"8c178dec-d468-439d-aef4-0f7d38b23ab3"],
Cell[6420, 178, 616, 9, 35, "Text",ExpressionUUID->"3c360039-72e7-47b6-a45f-93861380542b"],
Cell[7039, 189, 979, 28, 75, "Text",ExpressionUUID->"90595880-da00-429c-89bd-765e2feda9c7"],
Cell[CellGroupData[{
Cell[8043, 221, 1173, 36, 101, "Input",ExpressionUUID->"be8415f6-d1bb-40ce-b7c9-f29341beee42"],
Cell[9219, 259, 758, 23, 62, "Print",ExpressionUUID->"e950f94e-2166-4178-a1f9-c059fcf1861b"]
}, {2}]],
Cell[CellGroupData[{
Cell[10011, 287, 768, 16, 57, "Input",ExpressionUUID->"bcc88dc9-9c9c-4a40-bef0-68881058ab69"],
Cell[10782, 305, 1443, 32, 62, "Print",ExpressionUUID->"4f991c7d-3d7a-4015-8e2e-9f8670698630"]
}, {2}]],
Cell[12237, 340, 604, 12, 35, "Text",ExpressionUUID->"b77b38aa-65de-41d7-a089-623c50e849c2"]
}, Open  ]]
}, Open  ]]
}
]
*)

